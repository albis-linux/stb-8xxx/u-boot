/* types.h
 *	Copyright (C) 1999 Werner Koch (dd9jn).
 *
 * This file is part of SFSV.
 *
 * SFSV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SFSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#ifndef TYPES_H
#define TYPES_H

#include "gpgconfig.h"

#ifndef __KERNEL__
#include <sys/types.h>
#endif 

#ifndef HAVE_BYTE_TYPEDEF
  #undef byte
  typedef unsigned char byte;
  #define HAVE_BYTE_TYPEDEF
#endif

#ifndef HAVE_ULONG_TYPEDEF
  #undef ulong
  typedef unsigned long ulong;
  #define HAVE_ULONG_TYPEDEF
#endif

#ifndef HAVE_U32_TYPEDEF
  #undef u32
  #if SIZEOF_UNSIGNED_INT == 4
    typedef unsigned int u32;
  #elif SIZEOF_UNSIGNED_LONG == 4
    typedef unsigned long u32;
  #else
    #error no typedef for u32
  #endif
  #define HAVE_U32_TYPEDEF
#endif


#endif /*TYPES_H*/
