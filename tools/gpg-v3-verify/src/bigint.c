/* bigint.c
 *	Copyright (C) 1999 Werner Koch (dd9jn).
 *
 * This file is part of SFSV.
 *
 * SFSV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SFSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */


#include "gpgconfig.h"
#ifndef __KERNEL__
#include <assert.h>
#include <stdio.h>
#endif
#include "types.h"
#include "bigint.h"

#ifndef __KERNEL__
#include <string.h>
#endif

#ifdef __UBOOT_BUILD__
#include <malloc.h>
#include <common.h> /* strlen, memcmp */
#endif


/* M.Schenk 2013.11.04 for PBL */
#ifdef __PBL_BUILD__
#include <pre_string.h>
#endif

/* M.Schenk 2013.11.04, memcmp not defined in PBL */
#ifdef __PBL_BUILD__
static int memcmp(const void * m1, const void * m2, uint32_t size)
{
	int	diff ;
	const char * s1 = (const char *) m1;
	const char * s2 = (const char *) m2;


	while (size--)
	{
		diff = *s1++ - *s2++;

		if (diff != 0)
			return diff;
	}

	return 0;
}
#endif

static LIMB add_one_limb( LIMB *wp, LIMB *up, int un, LIMB v );
static LIMB add_n_limbs( LIMB *wp, LIMB *up, LIMB *vp, int n );
static LIMB add_limbs( LIMB *wp, LIMB *up, int un, LIMB *vp, int vn );

static LIMB sub_one_limb( LIMB *wp,  LIMB *up, int un, LIMB v );
static LIMB sub_n_limbs( LIMB *wp, LIMB *up, LIMB *vp, int n );
static LIMB sub_limbs( LIMB *wp, LIMB *up, int un, LIMB *vp, int vn );

static int  cmp_n_limbs( LIMB *up, LIMB *vp, int n );


#ifdef ENABLE_DEBUG
static struct {
  unsigned long add;
  unsigned long sub;
  unsigned long mul;
  unsigned long mulm;
  unsigned long mod;
  unsigned long lshift;
  unsigned long rshift;
  unsigned long add_1;
  unsigned long add_n;
  unsigned long sub_1;
  unsigned long sub_n;
  unsigned long normalize;
  unsigned long nbits;
  unsigned long test ;
} counters;
  #define INC_COUNTER(a)  do { counters. ## a ++; } while(0)
#else
  #define INC_COUNTER(a)
#endif


/**********************************************
 *************	helper functions **************
 **********************************************/

static void
clear_limbs( BIGINT *a )
{
    LIMB *ap = a->limbs;
    int i;

    for(i=0; i < BIGINT_MAX_LIMBS; i++ )
	ap[i] = 0;
    a->nlimbs = 0;
}

#ifdef ENABLE_DEBUG
static int dump_int( const char *text, BIGINT *a )
{
    printf( "%s: ", text );
    bi_print( stdout, a );
    putchar('\n');
}
#endif

#ifdef ENABLE_DEBUG
static int
dump_int2( const char *text, BIGINT *a )
{
    int i, j;
    printf( "%s: ", text );

    printf("alloc=%d sign=%d nlimbs=%d\n", a->allocated, a->sign, a->nlimbs );
    for(i=BIGINT_MAX_LIMBS-1,j=0 ; i >= 0; i-- ) {
	printf(" %08lX", a->limbs[i] );
	if( ++j > 7 ) {
	    putchar('\n');
	    j = 0;
	}
    }
    putchar('\n');
}
#endif

static inline void
normalize( BIGINT *a )
{
    LIMB *ap = a->limbs;

    INC_COUNTER(normalize);
    for( ; a->nlimbs > 0 && !ap[a->nlimbs-1]; a->nlimbs-- )
	;
}


/****************
 * Make sure that A as at least NLIMBS and initialize A zo zero
 */
void
bi_init( BIGINT *a, int nlimbs )
{
    /* fixme: handle nlimbs */
    a->sign = 0;
    a->nlimbs = 0;
    clear_limbs( a );
}


void
bi_setui( BIGINT *a, unsigned int value )
{
    /* we assume that value fits into a limb - this is reasonable
     * beucase this function is always used for small numbers */
    clear_limbs( a );
    a->sign = 0;
    a->nlimbs = 1;
    a->limbs[0] = value;
}

void
bi_set( BIGINT *a, BIGINT *b )
{
    clear_limbs( a );
    a->sign = b->sign;
    a->nlimbs = b->nlimbs;
    memcpy( a->limbs, b->limbs, b->nlimbs*BYTES_PER_LIMB );
}

/****************
 * Return the actual number of bits
 * This function also normalizes the value.
 */
int
bi_nbits( BIGINT *a )
{
    INC_COUNTER(nbits);
    for( ; a->nlimbs; a->nlimbs-- ) {
	unsigned int mask;
	int i;
	LIMB limb = a->limbs[a->nlimbs-1];

	for( i=0, mask = 1 << (BITS_PER_LIMB-1); mask; mask >>= 1, i++ ) {
	    if( (limb & mask) )
		return (a->nlimbs)*BITS_PER_LIMB - i;
	}
    }
    return 0;
}


/****************
 * Test whether bit number N is set.
 */
int
bi_test( BIGINT *a, int n )
{
    int i, mask;

    INC_COUNTER(test);
    i = n / BITS_PER_LIMB;
    mask = 1 << (n % BITS_PER_LIMB);
    return  (i >= a->nlimbs)? 0 : (a->limbs[i] & mask);
}

void
bi_setbit( BIGINT *a, int n )
{
    int i, mask;

    i = n / BITS_PER_LIMB;
    mask = 1 << (n % BITS_PER_LIMB);
    while( i >= a->nlimbs ) {
	a->limbs[i] = 0;
	a->nlimbs++;
    }
    a->limbs[i] |= mask;
}


int
bi_iszero( BIGINT *a )
{
    normalize( a );
    return !a->nlimbs;
}

int
bi_isneg( BIGINT *a )
{
    return a->nlimbs && a->sign;
}

int
bi_isodd( BIGINT *a )
{
    return a->nlimbs? (a->limbs[0] & 1) : 0;
}


void
bi_lshift( BIGINT *a )
{
    LIMB *ap = a->limbs;
    int n = a->nlimbs;
  #define highbit  ((LIMB)1 << (BITS_PER_LIMB-1))

    INC_COUNTER(lshift);
    if( !n )
	return;
    n--;

    if( (ap[n] & highbit) ) {
	a->nlimbs++;
	ap[n+1] = 1;
    }
    ap[n] <<= 1;
    for(n--; n >= 0; n-- ) {
	if( (ap[n] & highbit) )
	    ap[n+1] |= 1;
	ap[n] <<= 1;
    }

  #undef highbit
}

void
bi_rshift( BIGINT *a )
{
  #define highbit  ((LIMB)1 << (BITS_PER_LIMB-1))
    LIMB *ap = a->limbs;
    int n = a->nlimbs;
    int i;

    INC_COUNTER(rshift);
    if( !n )
	return;

    n--;
    for(i=0; i < n; i++, ap++ ) {
	*ap >>= 1;
	if( (ap[1] & 1) )
	    *ap |= highbit;
    }
    *ap >>= 1;
    if( !*ap )
	a->nlimbs--;
  #undef highbit
}

static void
bi_rshift_limb( BIGINT *a, int count )
{
    LIMB *ap = a->limbs;
    int n = a->nlimbs;
    int i;

    if( count >= n ) {
	ap[0] = 0;
	a->nlimbs = 0;
	return;
    }

    for(i=0; i < n-count; i++ )
	ap[i] = ap[i+count];
    ap[i] = 0; /* not really needed */
    a->nlimbs -= count;
}

/**********************************************
 ****************  adding  ********************
 **********************************************/

static LIMB
add_one_limb( LIMB *wp, LIMB *up, int un, LIMB v )
{
    LIMB tmp;

    INC_COUNTER(add_1);
    tmp = *up++;
    v += tmp;
    *wp++ = v;
    if( v < tmp ) { /* result is lower */
	while( --un ) {  /* add carries */
	    tmp = *up++ + 1;
	    *wp++ = tmp;
	    if( tmp )
		goto leave;
	}
	return 1; /* carry */
    }

  leave:
    if( wp != up )
	memcpy( wp, up, (un-1)*BYTES_PER_LIMB );
    return 0; /* no carry */
}


/* note: this function is able to do in situ addition */
static LIMB
add_n_limbs( LIMB *wp, LIMB *up, LIMB *vp, int n )
{
    LIMB cy = 0;
    LIMB a, b;
    int i;

    INC_COUNTER(add_n);
    for( i=0; i < n; i++ ) {
	a = *up++;
	b = *vp++;
	a += cy;	/* add carry from previous limb */
	cy = a < cy;	/* set new carry */
	a += b; 	/* add the other */
	cy += a < b;	/* update carry for this */
	*wp++ = a;
    }

    return cy;
}


/****************
 * Add a couple of limbs and return the carry.
 * Requires that UN >= VN
 */
static LIMB
add_limbs( LIMB *wp, LIMB *up, int un, LIMB *vp, int vn )
{
    LIMB cy = 0;

    if( vn )
	cy = add_n_limbs( wp, up, vp, vn );
    if( un - vn )
	cy = add_one_limb( wp + vn, up + vn, un - vn, cy );
    return cy;
}



static LIMB
sub_one_limb( LIMB *wp,  LIMB *up, int un, LIMB v )
{
    LIMB tmp;

    INC_COUNTER(sub_1);
    tmp = *up++;
    v = tmp - v;
    *wp++ = v;
    if( v > tmp ) {
	while( --un ) {
	    tmp = *up++;
	    *wp++ = tmp - 1;
	    if( tmp )
		goto leave;
	}
	return 1;
    }

  leave:
    if( wp != up )
	memcpy( wp, up, (un-1)*BYTES_PER_LIMB );
    return 0;
}



static LIMB
sub_n_limbs( LIMB *wp, LIMB *up, LIMB *vp, int n )
{
    LIMB cy = 0;
    LIMB a,b;
    int i;

    INC_COUNTER(sub_n);
    for( i=0; i < n; i++ ) {
	a = *up++;
	b = *vp++;
	b += cy;	/* add carry from previous limb */
	cy = b < cy;	/* set new carry */
	b  = a - b;	/* subtract */
	cy += b > a;	/* update carry for this */
	*wp++ = b;
    }

    return cy;
}


/****************
 * Subtract a couple of limbs and return the carry.
 * Requires that UN >= VN
 */
static LIMB
sub_limbs( LIMB *wp, LIMB *up, int un, LIMB *vp, int vn )
{
    LIMB cy = 0;

    if( vn )
	cy = sub_n_limbs( wp, up, vp, vn );
    if( un - vn )
	cy = sub_one_limb( wp + vn, up + vn, un - vn, cy );
    return cy;
}

/****************
 * Compute: W = U + V.
 * We assume that u and v are normalized and that w provides enough space.
 */
void
bi_add( BIGINT *w, BIGINT *u, BIGINT *v )
{
    LIMB *up, *vp;
    int  un, us, vn, vs;

    INC_COUNTER(add);
    if( u->nlimbs < v->nlimbs ) { /* swap U and V. */
	up = v->limbs;
	un = v->nlimbs;
	us = v->sign;
	vp = u->limbs;
	vn = u->nlimbs;
	vs = u->sign;
    }
    else {
	up = u->limbs;
	un = u->nlimbs;
	us = u->sign;
	vp = v->limbs;
	vn = v->nlimbs;
	vs = v->sign;
    }

    w->sign = 0;

    if( !vn ) {  /* v is zero: how easy */
	memcpy( w->limbs, up, un*BYTES_PER_LIMB );
	w->nlimbs = un;
	w->sign   = us;
	return;
    }


    if( us == vs ) {  /* same sign */
	LIMB cy;

	cy = add_limbs( w->limbs, up, un, vp, vn);
	w->limbs[un] = cy;
	w->nlimbs = un + cy;  /* carry is one or zero */
	w->sign = us;
	return;
    }

    /* the signs are different */
    if( un != vn ) {  /* note: un is always >= vn */
	sub_limbs( w->limbs, up, un, vp, vn );
	w->nlimbs = un;
	normalize(w);
	w->sign = us;
	return;
    }

    /* they have both the same number of limbs */
    if( cmp_n_limbs( up, vp, un ) < 0 ) { /* U is less than V */
	sub_n_limbs( w->limbs, vp, up, un );
	w->nlimbs = un;
	w->sign = !us;
    }
    else {  /* U is >= V */
	sub_n_limbs( w->limbs, up, vp, un );
	w->nlimbs = un;
	w->sign = us;
    }
    normalize( w );
}


void
bi_inc( BIGINT *a )
{
    BIGINT tmp, one;

    bi_init( &tmp, bi_nlimbs(a)+1 );
    bi_setui( &one, 1);
    bi_add( &tmp, a, &one );
    bi_set( a, &tmp );
}



/****************
 * Compute: W = U - V.
 * We assume that u and v are normalized and that w provides enough space.
 */
void
bi_sub( BIGINT *w, BIGINT *u, BIGINT *v )
{
    v->sign = !v->sign;
    bi_add( w, u, v );
    v->sign = !v->sign;
}


/**********************************************
 ***************  multiplying  ****************
 **********************************************/
#undef UMUL32
#if BITS_PER_LIMB != 32 || !defined(__GNUC__)
  #warning cannot use the assembler functions
#elif defined( __i386__ )
  #define UMUL32(h, l, u, v)	__asm__ ("mull %3"           \
					 : "=a" ((u32)(l)),  \
					   "=d" ((u32)(h))   \
					 : "%0" ((u32)(u)),  \
					   "rm" ((u32)(v)))
#elif defined( _ARCH_PPC )
  #define UMUL32(h, l, u, v)	do { u32 _u = (u), _v = (v);	\
				    __asm__ ("mulhwu %0,%1,%2"  \
					     : "=r" ((u32)(h))  \
					     : "%r" (_u),       \
					       "r" (_v));       \
				    (l) = _u * _v;		\
				} while (0)
#elif defined(USE_ARM_OPTIMIZATION)
  #define UMUL32(xh, xl, a, b) \
  __asm__ ("umull %0,%1,%2,%3" : "=&r" (xl), "=&r" (xh) : "r" (a), "r" (b))
#else
/*  #warning we do not know a machine instruction for multiplying*/
  #define UMUL32(h, l, u, v )	do { u32 _u = (u), _v = (v);		\
				     u32 _ul, _uh, _vl, _vh;		\
				     u32 _w0, _w1, _w2, _w3;		\
				     _ul = _u & 0x0000ffff;		\
				     _uh = _u >> 16;			\
				     _vl = _v & 0x0000ffff;		\
				     _vh = _v >> 16;			\
				     _w0 = _ul * _vl;			\
				     _w1 = _ul * _vh + (_w0 >> 16);	\
				     _w2 = _uh * _vl;			\
				     _w3 = _uh * _vh;			\
				     _w1 += _w2;			\
				     if( _w1 < _w2)			\
					 _w3 += 0x00010000;		\
				     (h) = _w3 + (_w1 >> 16);		\
				     (l) = ((_w1 & 0x0000ffff) << 16)	\
					   + (_w0 & 0x0000ffff);	\
				} while (0)
#endif

#ifdef UMUL32
static LIMB
addmul_one_limb( LIMB *wp, LIMB *up, int un, LIMB v )
{
    LIMB cy=0, high, low, tmp;
    int i;

    for( i=0; i < un; i++ ) {
	UMUL32( high, low, up[i], v);
	low += cy;
	cy = high + (low < cy);
	tmp = wp[i];
	low += tmp;
	cy  += (low < tmp);
	wp[i] = low;
    }
    return cy;
}
#endif

/****************
 * Compute: W = U * V
 */
void
bi_mul( BIGINT *w, BIGINT *u, BIGINT *v )
{
  #if !defined( UMUL32 )
    int n;
    BIGINT tmp;
  #warning using slow multiplication

    INC_COUNTER(mul);
    bi_init( &tmp, bi_nlimbs(v)+1 );
    bi_setui( w, 0);
    for( n = bi_nbits(v)-1; n >= 0; n-- ) {
	bi_lshift( w );
	if( bi_test( v, n ) ) {
	    bi_add( &tmp, w, u );
	    bi_set( w, &tmp );
	}
    }
  #else
    int i;
    LIMB *wp, *up, *vp, cy, limb;
    int un, vn;

    INC_COUNTER(mul);
    if( u->nlimbs < v->nlimbs ) { /* swap U and V. */
	up = v->limbs;
	un = v->nlimbs;
	vp = u->limbs;
	vn = u->nlimbs;
    }
    else {
	up = u->limbs;
	un = u->nlimbs;
	vp = v->limbs;
	vn = v->nlimbs;
    }
    bi_setui( w, 0);
    wp = w->limbs;
    w->nlimbs = un + vn;
    for(i=0; i < un+vn; i++ )
	w->limbs[i] = 0;

    /* For each iteration in the outer loop, multiply one limb from
     * U with one limb from V, and add it to PROD.  */
    for( i=0; i < vn; i++ ) {
	limb = vp[i];
	if( !limb )
	    cy = 0;
	else if( limb == 1 )
	    cy = add_n_limbs( wp, wp, up, un );
	else
	    cy = addmul_one_limb( wp, up, un, limb);
	wp[un] = cy;
	wp++;
    }
    normalize(w);

  #endif
}

/****************
 * Compute: W = U * V mod M
 *
 * Well, this is horrible slow and does only work for positive numbers
 */
void
bi_mulm( BIGINT *w, BIGINT *u_o, BIGINT *v_o, BIGINT *m )
{
/**
 * M.Schenk 2013.11.12
 * As with DSA 2048, SHA-256 we need to use slow
 * code, otherwise it crash.
 */
#if 0
    int n;
    BIGINT tmp;
    BIGINT u, v;

    INC_COUNTER(mulm);
    bi_init( &u, bi_nlimbs(m) );
    bi_init( &v, bi_nlimbs(m) );
    bi_mod( &u, u_o, m );
    bi_mod( &v, v_o, m );

    bi_init( &tmp, bi_nlimbs(m)+1 );
    bi_setui( w, 0);
    for( n = bi_nbits(&v)-1; n >= 0; n-- ) {
	bi_lshift( w );
	if( bi_cmp( w, m ) >= 0 ) {
	    bi_sub( &tmp, w, m );
	    bi_set( w, &tmp );
	}

	if( bi_test( &v, n ) ) {
	    bi_add( &tmp, w, &u );
	    bi_set( w, &tmp );
	    if( bi_cmp( w, m ) >= 0 ) {
		bi_sub( &tmp, w, m );
		bi_set( w, &tmp );
	    }
	}
    }
 #else
    /* this one is much faster */
    BIGINT tmp;

    INC_COUNTER(mulm);
    bi_mul( &tmp, u_o, v_o );
    bi_mod( w, &tmp, m );
 #endif
}

/**********************************************
 ***************  modulo **********************
 **********************************************/

/* Barret reduction: We assume that these conditions are met:
 * Given x =(x_2k-1 ...x_0)_b
 *	 m =(m_k-1 ....m_0)_b	  with m_k-1 != 0
 *	 y = floor(b^(2k) / m)
 * Output r = x mod m
 */
static void
barret( BIGINT *r, BIGINT *x, BIGINT *m )
{
    int i, k;
    BIGINT tmp, q1, q2, q3, r1, r2, y;
  #ifdef _REENTRANT
    #warning Barrett reduction is not very useful if we must be reentrant
    #warning however, this can be fixed and it is even possible
    #warning to make a DSA verification faster by precalculating y and
    #warning storing it along with the public key.
  #else
    static BIGINT last_m;
    static BIGINT last_y;
  #endif

    k = m->nlimbs;
    bi_init( &tmp, 0 );
    bi_setui( r, 0 );
    bi_setui( &q1, 0 );
    bi_setui( &q2, 0 );
    bi_setui( &q3, 0 );
    bi_setui( &r1, 0 );
    bi_setui( &r2, 0 );
    bi_setui( &y, 0 );
    /* Do precalculation: y = floor(b^(2k) / m) */
  #ifndef _REENTRANT
    if( last_m.nlimbs == m->nlimbs
	&& !memcmp( last_m.limbs, m->limbs, m->nlimbs*BYTES_PER_LIMB ) )
	bi_set( &y, &last_y );
    else {
  #endif
	bi_setui( &tmp, 1 );
	for(i=0; i < 32*(2*k); i++ )
	    bi_lshift( &tmp );
	bi_div( &y, &tmp, m );
  #ifndef _REENTRANT
	bi_set( &last_m, m );
	bi_set( &last_y, &y );
    }
  #endif
    /* 1. q1 = floor( x / b^k-1)
     *	  q2 = q1 * y
     *	  q3 = floor( q2 / b^k+1 )
     */
    bi_set( &q1, x );
    bi_rshift_limb( &q1, k-1 );
    bi_mul( &q2, &q1, &y );
    bi_set( &q3, &q2 );
    bi_rshift_limb( &q3, k+1 );

    /* 2. r1 = x mod b^k+1
     *	  r2 = q3 * m mod b^k+1
     *	  r  = r1 - r2
     * 3. if r < 0 then  r = r + b^k+1
     */
    bi_set( &r1, x );
    if( r1.nlimbs > k+1 )
	r1.nlimbs = k+1 ;
    bi_mul( &r2, &q3, m );
    if( r2.nlimbs > k+1 )
	r2.nlimbs = k+1  ;
    bi_sub( r, &r1, &r2 );
    if( bi_isneg( r ) ) {
	BIGINT tmp2;

	bi_setui( &tmp2, 1 );
	for(i=0; i < 32*(k+1); i++ )
	    bi_lshift( &tmp2 );
	bi_add( &tmp, r, &tmp2 );
	bi_set( r, &tmp );
    }

    /* 4. while r >= m do r = r - m */
    while( bi_cmp( r, m ) >= 0 ) {
	bi_sub( &tmp, r, m );
	bi_set( r, &tmp );
    }
}


/****************
 * Compute:  W = U mod M
 */
void
bi_mod( BIGINT *w, BIGINT *u, BIGINT *m )
{
    int n;
    BIGINT tmp;

    INC_COUNTER(mod);
    normalize( u );
    normalize( m );

    if( u->nlimbs <= 2*m->nlimbs ) {
	barret( w, u, m );
	return;
    }

    n = bi_cmp( u, m );
    if( !n ) {
	bi_setui( w, 0 );
	return;
    }
    if( n < 0 ) {
	bi_set( w, u);
	return;
    }

    bi_setui( w, 0 );
    bi_init( &tmp, bi_nlimbs(m) );

    for( n = bi_nbits(u)-1; n >= 0; n-- ) {
	bi_lshift( w );
	if( bi_test( u, n ) )
	    w->limbs[0] |= 1;
	if( bi_cmp( w, m ) >= 0 ) {
	    bi_sub( &tmp, w, m );
	    bi_set( w, &tmp );
	}
    }
}

/****************
 * Compute:  W = U / M
 */
void
bi_div( BIGINT *w, BIGINT *u, BIGINT *m )
{
    int n;
    BIGINT tmp, rem;

    normalize(u);
    normalize(m);
    n = bi_cmp( u, m );
    if( !n ) {
	bi_setui( w, 0 );
	return;
    }
    if( n < 0 ) {
	bi_set( w, u);
	return;
    }

    clear_limbs( w );
    bi_init( &rem, bi_nlimbs(m) );
    bi_init( &tmp, bi_nlimbs(m) );
    bi_setui( &rem , 0 );

    for( n = bi_nbits(u)-1; n >= 0; n-- ) {
	bi_lshift( &rem );
	if( bi_test( u, n ) )
	    rem.limbs[0] |= 1;
	if( bi_cmp( &rem, m ) >= 0 ) {
	    bi_sub( &tmp, &rem, m );
	    bi_set( &rem, &tmp );
	    bi_setbit( w, n );
	}
    }
}


/**********************************************
 ***************  inverting  ******************
 **********************************************/

/****************
 * Compute: W = U**(-1) mod M
 *
 * (TAOPC Vol II, 4.5.2, Alg X with Penk's modification)
 */
void
bi_invm( BIGINT *w, BIGINT *u_orig, BIGINT *m )
{
    BIGINT u, v, u1, u2, u3, v1, v2, v3, t1, t2, t3, tmp;
    unsigned int k;

    k = bi_nlimbs( m );
  #define X(a)	bi_init( &a, k );
    X(u); X(v); X(u1); X(u2); X(u3); X(v1); X(v2); X(v3); X(t1); X(t2); X(t3);
    X(tmp);
  #undef X

    bi_set( &u, u_orig);
    bi_set( &v, m);

    for(k=0; !bi_test( &u,0) && !bi_test( &v,0); k++ ) {
	bi_rshift( &u );
	bi_rshift( &v );
    }

    bi_setui( &u1, 1 );
    bi_set( &u3, &u );
    bi_set( &v1, &v );
    bi_sub( &v2, &u1, &u ); /* v2 = 1 - u */
    bi_set( &v3, &v );
    if( bi_isodd( &u ) ) {
	bi_setui( &t1, 0);
	bi_setui( &t2, 1 );
	t2.sign = 1;
	bi_set( &t3, &v );
	t3.sign = !t3.sign;
	goto Y4;
    }

    bi_setui( &t1, 1 );
    bi_set( &t3, &u );

    do {
	do {
	    if( bi_isodd( &t1 ) || bi_isodd( &t2 ) ) { /* one is odd */
		bi_add( &tmp, &t1, &v); bi_set( &t1, &tmp );
		bi_sub( &tmp, &t2, &u); bi_set( &t2, &tmp );
	    }
	    bi_rshift( &t1 );
	    bi_rshift( &t2 );
	    bi_rshift( &t3 );
	  Y4:
	    ;
	} while( !bi_isodd( &t3 ) ); /* while t3 is even */

	if( !t3.sign ) {
	    bi_set( &u1, &t1 );
	    bi_set( &u2, &t2 );
	    bi_set( &u3, &t3 );
	}
	else {
	    int sign;

	    bi_sub( &v1, &v, &t1 );
	    sign = u.sign;
	    u.sign = !u.sign;
	    bi_sub( &v2, &u, &t2 );
	    u.sign = sign;
	    sign = t3.sign;
	    t3.sign = !t3.sign;
	    bi_set( &v3, &t3 );
	    t3.sign = sign;
	}
	bi_sub( &t1, &u1, &v1 );
	bi_sub( &t2, &u2, &v2 );
	bi_sub( &t3, &u3, &v3 );
	if( t1.sign ) {
	    bi_add( &tmp, &t1, &v); bi_set( &t1, &tmp );
	    bi_sub( &tmp, &t2, &u ); bi_set( &t2, &tmp );
	}
    } while( !bi_iszero( &t3 ) );

    bi_set( w, &u1 );
}

/**********************************************
 *************	exponentiation	***************
 **********************************************/

/****************
 * Compute: W = G ** E mod M
 */
#if 0
void
bi_expm( BIGINT *w, BIGINT *g, BIGINT *e, BIGINT *m )
{
    // PETERVOSER assert(0);
}
#endif

/****************
 * Compute: W = G[0]**E[0] * ... * G[N-1]**E[N-1] mod M
 */
void
bi_mulexpm( BIGINT *w, BIGINT **g, BIGINT **e, int n, BIGINT *m )
{
    BIGINT tbl[4];	 /* precomputation table (size = 2^n) */
    char   tbl_init[4];  /* whether table is initialized */
    int maxbits;	 /* of largest exponent */
    unsigned int idx;
    BIGINT tmp;
    int i, j;

    bi_init( &tmp, bi_nlimbs(m)+1 );
    memset( tbl_init, 0, 4 );
    // PETERVOSER assert( n && n < 3 );

    for( maxbits=0, i=0; i < n; i++ ) {
	j = bi_nbits( e[i] );
	if( j > maxbits )
	    maxbits = j;
    }

    bi_setui( w, 1 );
    for(i = 1; i <= maxbits; i++ ) {
	bi_mulm( &tmp, w, w, m );
	for(idx=0, j = n-1; j >= 0; j-- ) {
	    idx <<= 1;
	    if( bi_test( e[j], maxbits-i ) )
		idx |= 1;
	}

	if( !tbl_init[idx] ) {
	    if( !idx ) {
		bi_setui( tbl+0, 1 );
		tbl_init[idx] = 1;
	    }
	    else {
		for(j=0; j < n; j++ ) {
		    if( (idx & (1<<j) ) ) {
			if( !tbl_init[idx] ) {
			    bi_set( tbl+idx, g[j] );
			    tbl_init[idx] = 1;
			}
			else {
			    bi_mulm( tbl+idx, tbl+idx, g[j], m );
			}
		    }
		}
		if( !tbl_init[idx] ) {
		    bi_setui( tbl+idx, 0 );
		    tbl_init[idx] = 1;
		}
	    }
	}
	bi_mulm( w, &tmp, tbl+idx, m );
    }
}

/**********************************************
 *************	compare  **********************
 **********************************************/

/****************
 * Compare to limb arrays of size N
 * Returns <0 if up < vp
 *	    0 if up == vp
 *	   >0 if up > vp
 */
static int
cmp_n_limbs( LIMB *up, LIMB *vp, int n )
{
    for( n--; n >= 0 ; n-- ) {
	if( up[n] != vp[n] )
	    return up[n] < vp[n] ? -1 : 1;
    }
    return 0;
}

/****************
 * Returns <0 if a < b
 *	    0 if a == b
 *	   >0 if a > b
 * As always, we assume that the operands are normalized
 */
int
bi_cmp( BIGINT *a, BIGINT *b )
{
    int rc;
    if( !a->sign && b->sign )
	return 1;
    if( a->sign && !b->sign )
	return -1;

    if( a->nlimbs != b->nlimbs ) {
	if( !a->sign && !b->sign )
	    return a->nlimbs - b->nlimbs;
	return b->nlimbs + a->nlimbs;
    }

    if( !a->nlimbs )
	return 0;

    rc = cmp_n_limbs( a->limbs, b->limbs, a->nlimbs );
    if( !rc ){
    	return 0; /* same */
    }
    else if( a->sign ){
	return rc < 0? 1 : -1;
    }
    else {    
	return rc < 0? -1 : 1;
    }
}

/**********************************************
 *************	input/output  *****************
 **********************************************/

#ifdef ENABLE_DEBUG
void
bi_print( FILE *fp, BIGINT *a )
{
    int i, n=0;

    if( a->sign )
	putc('-', fp);
    if( !a->nlimbs )
	putc('0', fp );
  #if BYTES_PER_LIMB == 2
    #define X "4"
  #elif BYTES_PER_LIMB == 4
    #define X "8"
  #elif BYTES_PER_LIMB == 8
    #define X "16"
  #else
    #error please define the format here
  #endif
    for(i=a->nlimbs; i > 0 ; i-- )
	fprintf(fp, i != a->nlimbs ? "%0" X "lX":"%lX",
			    (unsigned long)a->limbs[i-1]);
  #undef X
}
#endif


/****************
 * convert a big intger from external representation to
 * the internal one.  The OpenPGP format is used.
 * Returns: number of bytes read or -1 in case of error.
 */
int
bi_read( BIGINT *w, const byte *buf, size_t buflen )
{
    int i, j;
    int nbits, nbytes, nlimbs;
    size_t nread;

    if( buflen < 2 )
	return -1;
    nbits = buf[0] << 8 | buf[1];
    if( nbits<0 || nbits > BIGINT_MAX_BITS-128 ) /* 128 used as safety margin */
	return -1;  /* too long */
    buf += 2;
    nread = 2;

    nbytes = (nbits+7) / 8;
    nlimbs = (nbytes+BYTES_PER_LIMB-1) / BYTES_PER_LIMB;
    bi_init( w, nlimbs );

    i = BYTES_PER_LIMB - nbytes % BYTES_PER_LIMB;
    i %= BYTES_PER_LIMB;
    j = w->nlimbs = nlimbs;
    w->sign = 0;
    for( ; j > 0; j-- ) {
	LIMB limb = 0;
	for(; i < BYTES_PER_LIMB; i++ ) {
	    if( ++nread > buflen )
		return -1;
	    limb <<= 8;
	    limb |= *buf++;
	}
	i = 0;
	w->limbs[j-1] = limb;
    }

    return nread;
}


int
bi_scan( BIGINT *w, const char *s )
{
    int prepend_zero=0, i, j, c, c1, c2;
    unsigned int nbits, nbytes;
    LIMB a;

    if( *s == '-' ) {
	w->sign = 1;
	s++;
    }
    else
	w->sign = 0;

    if(!(*s == '0' && s[1] == 'x'))	return -1; /* we can only handle hex strings */

    s++; s++;

    nbits = strlen(s)*4;
    if( (nbits % 8) )
	prepend_zero = 1;
    nbytes = (nbits+7) / 8;
    w->nlimbs = (nbytes+BYTES_PER_LIMB-1) / BYTES_PER_LIMB;
    i = (BYTES_PER_LIMB - (nbytes % BYTES_PER_LIMB)) % BYTES_PER_LIMB;
    for(j=w->nlimbs ; j > 0; j-- ) {
	a = 0;
	for(; i < BYTES_PER_LIMB; i++ ) {
	    if( prepend_zero ) {
		c1 = '0';
		prepend_zero = 0;
	    }
	    else
		c1 = *s++;
	    c2 = *s++;
	    if( c1 >= '0' && c1 <= '9' )
		c = c1 - '0';
	    else if( c1 >= 'a' && c1 <= 'f' )
		c = c1 - 'a' + 10;
	    else if( c1 >= 'A' && c1 <= 'F' )
		c = c1 - 'A' + 10;
	    else
		return -1;
	    c <<= 4;
	    if( c2 >= '0' && c2 <= '9' )
		c |= c2 - '0';
	    else if( c2 >= 'a' && c2 <= 'f' )
		c |= c2 - 'a' + 10;
	    else if( c2 >= 'A' && c2 <= 'F' )
		c |= c2 - 'A' + 10;
	    else
		return -1;
	    a <<= 8;
	    a |= c;
	}
	i = 0;
	w->limbs[j-1] = a;
    }

    return 0;
}
#if 0
void
bi_stat()
{
  #ifdef ENABLE_DEBUG
    printf("bi_add_1 : %8lu\n", counters.add_1 );
    printf("bi_add_n : %8lu\n", counters.add_n );
    printf("bi_sub_1 : %8lu\n", counters.sub_1 );
    printf("bi_sub_n : %8lu\n", counters.sub_n );
    printf("bi_add   : %8lu\n", counters.add );
    printf("bi_mul   : %8lu\n", counters.mul  );
    printf("bi_mulm  : %8lu\n", counters.mulm );
    printf("bi_mod   : %8lu\n", counters.mod  );
    printf("bi_lshift: %8lu\n", counters.lshift );
    printf("bi_rshift: %8lu\n", counters.rshift );
    printf("normalize: %8lu\n", counters.normalize );
    printf("nbits    : %8lu\n", counters.nbits     );
    printf("test     : %8lu\n", counters.test      );
  #endif
}
#endif
