
#ifndef __KERNEL__
#include <stdio.h>
#endif

#ifdef __PBL_BUILD__
#	include <pbl_crt.h>

#elif defined(__APPLET_BUILD__)
#	include <stdarg.h>
	extern void serial_printf(const char* format, ...);

#elif defined(__UBOOT_BUILD__)
#	include <config.h> //secure/not secure for U-boot build
	extern void serial_printf(const char* format, ...);

#elif defined(__APPLICATION_BUILD__)

#else
#error "seems to be build outside PBL,Applet,or Uboot"
#endif

#include "gpg-v3-verify.h"
#ifndef __KERNEL__
#include "types.h"
#endif
#include "packet.h"
#include "sha1.h"
#include "sha256.h"
#include "dsa.h"

int readHashID(const char *sigData, unsigned int sigSize, int *hashID);

#if defined(__PBL_BUILD__) || defined(__UBOOT_BUILD__) || defined(__APPLICATION_BUILD__)
/*=============================================================================
  1) Calculate the SHA1 hash of the encrypted initramfs
  2) Provide the "SHA1 hash of the encrypted image" and "DSA from the header" to dsa.c for verification
  =============================================================================*/
int verifyGpgV3SignatureSHA1(char *imageToVerify,
                             unsigned int imageToVerifySize,
                             char *signature,
                             unsigned int signatureSize)
{
    int n;
    const char *data;
    size_t pktlen;
    int lenbytes;
    SIG_packet sig;
    SHA1_CONTEXT sha1_ctx;

    /* Calculate the SHA1 hash of the encrypted initramfs */
    /* start digest calculation */
    sha1_init( &sha1_ctx );
    sha1_write( &sha1_ctx, (const byte *) imageToVerify, imageToVerifySize);


    /* Parse the Signature packet and check if it contains valid DSA Signature and SHA1 hash */
    /* parse the sig packet */
    data = signature;
    /* we assume that this is a rfc2440 v3 signature */
    if( !signatureSize || !(*data & 0x80) )
	return SFSV_NO_SIG;

    if( (*data & 0x40) )
	return SFSV_BAD_PGP;  /* new CTB mode is not supported */
    if( ((*data>>2)&0xf) != 2 )
	return SFSV_BAD_PGP;  /* not a signature packet */
    lenbytes = ((*data&3)==3)? 0 : (1<<(*data & 3));
    data++; signatureSize--;
    if( !lenbytes || signatureSize < lenbytes )
	return SFSV_BAD_PGP;  /* unsupported length header or packet too short*/
    for( pktlen=0 ; lenbytes; lenbytes-- ) {
	pktlen <<= 8;
	pktlen |= *data++; signatureSize--;
    }
    if( pktlen > signatureSize || pktlen < 16 )
	return SFSV_BAD_PGP;  /* packet is too large or too short */

    if( *data != 3 )
	return SFSV_BAD_PGP;  /* only v3 signatures are supported */
    data++; signatureSize--;
    data++; signatureSize--; /* skip m5 len */

    sha1_write( &sha1_ctx,(const byte *) data, 1 );  /* hash the sig class */
    if( *data )
	return SFSV_BAD_PGP;  /* not a sig class 0 */
    data++; signatureSize--;

    sha1_write( &sha1_ctx,(const byte *) data, 4 );  /* hash the timestamp */
    sig.timestamp = read_32((const byte *) data); data+=4; signatureSize-=4;

    sig.keyid[0] = read_32((const byte *) data); data+=4; signatureSize-=4;
    sig.keyid[1] = read_32((const byte *) data); data+=4; signatureSize-=4;

    if( *data != 17 )
	return SFSV_BAD_PGP;  /* not a DSA sig */
    data++; signatureSize--;
    if( *data != 2 )
	return SFSV_BAD_PGP;  /* not a SHA1 hash */
    data++; signatureSize--;

    if( pktlen < 6 )
	return SFSV_BAD_PGP;  /* packet is too short */

    data++; signatureSize--;	/* no need for the digest start bytes */
    data++; signatureSize--;

    n = bi_read( &sig.r, data, signatureSize );
    if( n <= 0 )
	return -1; /* bad MPI */
    data += n; signatureSize -= n;

    n = bi_read( &sig.s, data, signatureSize );
    if( n <= 0 )
	return -1; /* bad MPI */

    /* sig has now all the required data */
    sha1_final( &sha1_ctx );

#if 0
    return verify_signature( sha1_read( &sha1_ctx ), &sig );
#else
    return verify_signature( sha1_read( &sha1_ctx ), 160, &sig );
#endif
}

#if defined(__STB_SECURE__)
/**
 * Advanced with SHA-256 Hash
 */
int verifyGpgV3SignatureSHA256(char *imageToVerify,
                               unsigned int imageToVerifySize,
                               char *signature,
                               unsigned int signatureSize)
{
    int n;
    const char *data;
    size_t pktlen;
    int lenbytes;
    SIG_packet sig;
    uint8 digest[32];
    sha256_context sha256_ctx;

    /* Calculate the SHA256 hash of the encrypted initramfs */
    /* start digest calculation */
    sha256_starts( &sha256_ctx );
    sha256_update( &sha256_ctx, (uint8 *) imageToVerify, imageToVerifySize);

    /* see http://www.ietf.org/rfc/rfc4880.txt */

    /* Parse the Signature packet and check if it contains valid DSA Signature and SHA1 hash */
    /* parse the sig packet */
    data = signature;
    /* we assume that this is a rfc2440 v3 signature */
    if( !signatureSize || !(*data & 0x80) )
	return SFSV_NO_SIG;

    if( (*data & 0x40) ) {
    	LOGERROR("SFSV_BAD_PGP 1\n");
    	return SFSV_BAD_PGP;  /* new CTB mode is not supported */
    }

    if( ((*data>>2)&0xf) != 2 ) {
    	LOGERROR("SFSV_BAD_PGP 2\n");
    	return SFSV_BAD_PGP;  /* not a signature packet */
    }



    lenbytes = ((*data&3)==3)? 0 : (1<<(*data & 3));
    data++; signatureSize--;
    if( !lenbytes || signatureSize < lenbytes ) {
    	LOGERROR("SFSV_BAD_PGP 3\n");
    	return SFSV_BAD_PGP;  /* unsupported length header or packet too short*/
    }

    for( pktlen=0 ; lenbytes; lenbytes-- ) {
	pktlen <<= 8;
	pktlen |= *data++; signatureSize--;
    }
    if( pktlen > signatureSize || pktlen < 16 ) {
    	LOGERROR("SFSV_BAD_PGP 4\n");
    	return SFSV_BAD_PGP;  /* packet is too large or too short */

    }

    if( *data != 3 ) {
    	LOGERROR("SFSV_BAD_PGP 5\n");
    	return SFSV_BAD_PGP;  /* only v3 signatures are supported */
    }

    data++; signatureSize--;
    data++; signatureSize--; /* skip m5 len */

    sha256_update( &sha256_ctx, (uint8 *) data, 1 );  /* hash the sig class */
    if( *data ) {
    	LOGERROR("SFSV_BAD_PGP 6\n");
	return SFSV_BAD_PGP;  /* not a sig class 0 */
    }
    data++; signatureSize--;

    sha256_update( &sha256_ctx, (uint8 *) data, 4 );  /* hash the timestamp */
    sig.timestamp = read_32((const byte *) data); data+=4; signatureSize-=4;

    sig.keyid[0] = read_32((const byte *) data); data+=4; signatureSize-=4;
    sig.keyid[1] = read_32((const byte *) data); data+=4; signatureSize-=4;

    if( *data != 17 ) {
    	LOGERROR("SFSV_BAD_PGP 7\n");
    	return SFSV_BAD_PGP;  /* not a DSA sig */
    }
    data++; signatureSize--;
    if( *data != 8 ) {
    	LOGERROR("SFSV_BAD_PGP 8 0x%x\n",  *data);
    	return SFSV_BAD_PGP;  /* not a SHA-256 hash */
    }
    data++; signatureSize--;

    if( pktlen < 6 ) {
    	LOGERROR("SFSV_BAD_PGP 9\n");
    	return SFSV_BAD_PGP;  /* packet is too short */
    }


    /* - Two-octet field holding left 16 bits of signed hash value. */

    data++; signatureSize--;	/* no need for the digest start bytes */
    data++; signatureSize--;

    n = bi_read( &sig.r, data, signatureSize );
    if( n <= 0 )
	return -1; /* bad MPI */
    data += n; signatureSize -= n;

    n = bi_read( &sig.s, data, signatureSize );
    if( n <= 0 )
	return -1; /* bad MPI */

    /* sig has now all the required data */
    sha256_finish( &sha256_ctx, digest );

    return verify_signature( digest, 256, &sig );
}
#endif /* __STB_SECURE__ */

int verifyGpgV3Signature(char *imageToVerify,
                         unsigned int imageToVerifySize,
                         char *signature,
                         unsigned int signatureSize)
{
	int ret = 0;
	int hashID = 0;

	LOGINFO("GPG Library ver. %s",__GPG_LIB_VERSION__);

	if(readHashID(signature, signatureSize, &hashID) != SFSV_GOOD_SIG)
	{
		return -1;
	}

	if(hashID == SHA1_SIG_ID)
	{
		LOGINFO("Check GPGV3 signature of image SHA-1");
		ret = verifyGpgV3SignatureSHA1(imageToVerify, imageToVerifySize,
									   signature, signatureSize);
		LOGINFO("Check GPGV3 signature of image done ret %d", ret);
	}
#if defined(__STB_SECURE__)
	else if(hashID == SHA256_SIG_ID)
	{
		LOGINFO("Check GPGV3 signature of image SHA-256");
		ret = verifyGpgV3SignatureSHA256(imageToVerify, imageToVerifySize,
				                         signature, signatureSize);
		LOGINFO("Check GPGV3 signature of image done ret %d", ret);
	}
#endif
	else
	{
		LOGINFO("verifyGpgV3Signature: bad GPG-header! [%x]", hashID);
		ret = SFSV_BAD_PGP;
	}

	return (ret == SFSV_GOOD_SIG) ? 0 : -1;
}
#endif /* __PBL_BUILD__ || __UBOOT_BUILD__ || __APPLICATION_BUILD__ */

#ifdef __APPLET_BUILD__

/*=============================================================================
  1) Verify provided signature
  2) Send pre-calculated hash and signature to verification
  =============================================================================*/

int verifyGpgV3Hash(	 unsigned char *hash,
                         char *signature,
                         unsigned int signatureSize)
{
    int n;
    int sha;
    unsigned int lenbytes;
    const char *data;
    size_t pktlen;
    SIG_packet sig;

	LOGINFO("GPG Library ver. %s",__GPG_LIB_VERSION__);

    /* see http://www.ietf.org/rfc/rfc4880.txt */
    /* Parse the Signature packet and check if it contains valid DSA Signature and SHA1 hash */
    /* parse the sig packet */
    data = signature;
    /* we assume that this is a rfc2440 v3 signature */
    if( !signatureSize || !(*data & 0x80) )
	return SFSV_NO_SIG;

    if( (*data & 0x40) ) {
    	LOGINFO("SFSV_BAD_PGP 1\n");
    	return SFSV_BAD_PGP;  /* new CTB mode is not supported */
    }

    if( ((*data>>2)&0xf) != 2 ) {
    	LOGINFO("SFSV_BAD_PGP 2\n");
    	return SFSV_BAD_PGP;  /* not a signature packet */
    }

    lenbytes = ((*data&3)==3)? 0 : (1<<(*data & 3));
    data++; signatureSize--;
    if( !lenbytes || signatureSize < lenbytes ) {
    	LOGINFO("SFSV_BAD_PGP 3\n");
    	return SFSV_BAD_PGP;  /* unsupported length header or packet too short*/
    }

    for( pktlen=0 ; lenbytes; lenbytes-- ) {
	pktlen <<= 8;
	pktlen |= *data++; signatureSize--;
    }
    if( pktlen > signatureSize || pktlen < 16 ) {
    	LOGINFO("SFSV_BAD_PGP 4\n");
    	return SFSV_BAD_PGP;  /* packet is too large or too short */

    }

    if( *data != 3 ) {
    	LOGINFO("SFSV_BAD_PGP 5\n");
    	return SFSV_BAD_PGP;  /* only v3 signatures are supported */
    }

    data++; signatureSize--;
    data++; signatureSize--; /* skip m5 len */

    if( *data ) {
    	LOGINFO("SFSV_BAD_PGP 6\n");
	return SFSV_BAD_PGP;  /* not a sig class 0 */
    }
    data++; signatureSize--;

    sig.timestamp = read_32((const byte *) data); data+=4; signatureSize-=4;

    sig.keyid[0] = read_32((const byte *) data); data+=4; signatureSize-=4;
    sig.keyid[1] = read_32((const byte *) data); data+=4; signatureSize-=4;

    if( *data != 17 ) {
    	LOGINFO("SFSV_BAD_PGP 7\n");
    	return SFSV_BAD_PGP;  /* not a DSA sig */
    }
    data++; signatureSize--;

    if( *data == 8 ) {
    	sha=256;
    	}
    else if( *data == 2 ) {
    	sha=160;
    	}
    else	{
    	return SFSV_BAD_PGP;  /* not SHA1 nor SHA256 hash */
    }

    data++; signatureSize--;

    if( pktlen < 6 ) {
    	LOGINFO("SFSV_BAD_PGP 9\n");
    	return SFSV_BAD_PGP;  /* packet is too short */
    }


    /* - Two-octet field holding left 16 bits of signed hash value. */
    data++; signatureSize--;	/* no need for the digest start bytes */
    data++; signatureSize--;

    n = bi_read( &sig.r, data, signatureSize );
    if( n <= 0 )
	return -1; /* bad MPI */
    data += n; signatureSize -= n;

    n = bi_read( &sig.s, data, signatureSize );
    if( n <= 0 )
	return -1; /* bad MPI */

    return verify_signature( hash, sha, &sig );
}
#endif /* __APPLET_BUILD__ */

/*
 * Function for reading the Hash algorithm type of the signature
 * *************************************************************
 *
 * Signature version 3 is according RFC4880 (obsoletes: RFC2440)
 *
 *  Version 3 Signature Packet Format (see RFC4880 chapter 5.2.2)
 *  =============================================================
 *  The body of a version 3 Signature Packet contains:
 *  - One-octet version number (3).
 *  - One-octet length of following hashed material.  MUST be 5.
 *  - One-octet signature type.
 *  - Four-octet creation time.
 *  - Eight-octet Key ID of signer.
 *  - One-octet public-key algorithm.
 *  - One-octet hash algorithm. ==>> offset in body = 17
 *                                   *******************
 *  - Two-octet field holding left 16 bits of signed hash value.
 *  - One or more multiprecision integers comprising the signature.
 *
 *  The Hash Algorithms types are defined in RFC4880 chapter 9.4
 *  ============================================================
 *  ID           Algorithm                             Text Name
 *  --           ---------                             ---------
 *  2          - SHA-1 [FIPS180]                       "SHA1"
 *  8          - SHA256 [FIPS180]                      "SHA256"
 */

int readHashID(const char *sigData, unsigned int sigSize, int *hashID)
{
	int retVal = 0;
	/*
	 * The value of *sigData & 3 is the length of the header (see RFC4880 chapter 4.2.1)
	 * 0 => the packet has one-octet length
	 * 1 => the packet has two-octet length
	 * 2 => the packet has four-octet length
	 * 3 => the packet is of indeterminate length
	 */
	unsigned int lenbytes = 0;
	int n = 0;
	size_t pktlen;
	SIG_packet sig;

    if( !sigSize || !(*sigData & 0x80) )
	return SFSV_NO_SIG;

    if( (*sigData & 0x40) ) {
    	LOGERROR("SFSV_BAD_PGP 1\n");
    	return SFSV_BAD_PGP;  /* new CTB mode is not supported */
    }

    if( ((*sigData>>2)&0xf) != 2 ) {
    	LOGERROR("SFSV_BAD_PGP 2\n");
    	return SFSV_BAD_PGP;  /* not a signature packet */
    }

    lenbytes = ((*sigData & 3) == 3) ? 0 : (1 << (*sigData & 3));
    sigData++;
    sigSize--;
    if( !lenbytes || sigSize < lenbytes ) {
    	LOGERROR("SFSV_BAD_PGP 3\n");
    	return SFSV_BAD_PGP;  /* unsupported length header or packet too short*/
    }

    for( pktlen=0 ; lenbytes; lenbytes-- ) {
	pktlen <<= 8;
	pktlen |= *sigData++;
	sigSize--;
    }
    if( pktlen > sigSize || pktlen < 16 ) {
    	LOGERROR("SFSV_BAD_PGP 4\n");
    	return SFSV_BAD_PGP;  /* packet is too large or too short */

    }

    if( *sigData != 3 ) {
    	LOGERROR("SFSV_BAD_PGP 5\n");
    	return SFSV_BAD_PGP;  /* only v3 signatures are supported */
    }

    sigData += 2;
    sigSize -= 2; /* skip m5 len */

    if( *sigData ) {
    	LOGERROR("SFSV_BAD_PGP 6\n");
	return SFSV_BAD_PGP;  /* not a sig class 0 */
    }

    sigData++;
    sigSize--;

    sig.timestamp = read_32((const byte *) sigData); sigData+=4; sigSize-=4;
    sig.keyid[0] = read_32((const byte *) sigData); sigData+=4; sigSize-=4;
    sig.keyid[1] = read_32((const byte *) sigData); sigData+=4; sigSize-=4;

    if( *sigData != 17 ) {
    	LOGERROR("SFSV_BAD_PGP 7\n");
    	return SFSV_BAD_PGP;  /* not a DSA sig */
    }
    sigData++;
    sigSize--;
    *hashID = (int)*sigData;
    if((*hashID != SHA1_SIG_ID) && (*hashID != SHA256_SIG_ID))
    {
    	return SFSV_BAD_PGP;
    }
    else
    {
    	retVal = SFSV_GOOD_SIG;
    }
    sigData++;
    sigSize--;

    if( pktlen < 6 ) {
    	LOGERROR("SFSV_BAD_PGP 9\n");
    	return SFSV_BAD_PGP;  /* packet is too short */
    }

    /* - Two-octet field holding left 16 bits of signed hash value. */
    /* no need for the digest start bytes */

    sigData += 2;
    sigSize -= 2;

    n = bi_read( &sig.r, sigData, sigSize );
    if( n <= 0 )
    {
    	return -1; /* bad MPI */
    }
    sigData += n; sigSize -= n;

    n = bi_read( &sig.s, sigData, sigSize );
    if( n <= 0 )
    {
    	return -1; /* bad MPI */
    }

	return retVal;
}
