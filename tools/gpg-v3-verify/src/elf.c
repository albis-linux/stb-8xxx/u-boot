/* elf.c - verification of an ELF rfc2440 signature
 *	Copyright (C) 1999 Werner Koch (dd9jn).
 *
 * This file is part of SFSV.
 *
 * SFSV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SFSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

/* How it works:
 *
 * The signature is put in a .note section with the name "rfc2440"
 * and a type of 1.  It contains the a valid rfc2440 signature and
 * is padded with zeroes.  The signature is calculated over the
 * complete ELF image including the .note(rfc2440,1) headers but
 * (of course) without the actual note descriptor as specified in
 * descsz (which is included in the hash).
 *
 * The notes section is first located by looking at the PHDRS table
 * and if it has not been found there by scanning the section table
 * for the first sction of type SHT_NOTE.
 */

#include "gpgconfig.h"
#ifndef __KERNEL__
#include <stdlib.h>
#include <stdio.h>
#endif
#include "types.h"
#include "gpg-v3-verify.h"


static const byte elf_magic[4] = { '\x7f', '\x45', '\x4c', '\x46' }; /*~ELF*/
static const byte note_name[8] = { '\x52', '\x46', '\x43', '\x32',
				   '\x34', '\x34', '\x30', '\0'  }; /*RFC2440*/

#define PT_NOTE   4
#define SHT_NOTE  7


#ifndef __KERNEL__

static size_t
get_u16( const byte *p, int big_endian )
{
    if( big_endian )
	return p[0]<<8 | p[1];
    else
	return p[1]<<8 | p[0];
}

static size_t
get_u32( const byte *p, int big_endian )
{
    if( big_endian )
	return p[0]<<24 | p[1] << 16 | p[2] << 8 | p[3];
    else
	return p[3]<<24 | p[2] << 16 | p[1] << 8 | p[0];
}

static int
parse_note( const byte *image, size_t off, size_t len, int big_endian,
	    size_t *sigoff, size_t *siglen )
{
    const byte *notep = image + off;

    while( len ) {
	size_t nl, dl, ty;
	if( len < 12 )
	    return SFSV_BAD_ELF; /* bad note section */
	nl = (get_u32( notep, big_endian )+3)&~0x03;
	dl = (get_u32( notep+4, big_endian )+3)&~0x03;
	ty = get_u32( notep+8, big_endian );
	len -= 12; notep += 12;
      #ifdef ENABLE_DEBUG
	fprintf(stderr, "  nl=%u  dl=%u  ty=%u  len=%u  name=`%s'\n", nl, dl, ty, len, notep );
      #endif
	if( nl > len || dl > len || (nl+dl) > len )
	    return SFSV_BAD_ELF; /* bad note section */

	if( nl >= 8 && !memcmp(notep, note_name, 8 ) && ty == 1 ) {
	    *sigoff = notep+nl - image;
	    *siglen = dl;
	    return 0;
	}
	notep += nl + dl; len -= nl+dl;
    }
    return SFSV_NO_SIG;
}

int
sfsv_locate_elf_signature( const byte *image, size_t imagelen,
			   size_t *sigoff, size_t *siglen )
{
    int big_endian = 0;
    size_t phoff, phentsize, phnum, off, len;
    size_t shoff, shentsize, shnum;
    const byte *ph, *sh;
    int rc;

    *sigoff = *siglen = 0;

    /* Do some very basic checks */
    if( imagelen < 16+9*4 )
	return SFSV_BAD_ELF; /* too short */
    if( memcmp( image, elf_magic, 4 ) )
	return SFSV_BAD_ELF; /* bad magic */
    if( image[4] != 1 )
	return SFSV_BAD_ELF; /* not 32 bit objects */
    if( image[6] != 1 )
	return SFSV_BAD_ELF; /* bad version */
    if( image[5] == 2 )
	big_endian = 1;
    else if( image[5] != 1 )
	return SFSV_BAD_ELF; /* bad data encoding */

    phoff     = get_u32( image+28, big_endian );
    shoff     = get_u32( image+32, big_endian );
    phentsize = get_u16( image+42, big_endian );
    phnum     = get_u16( image+44, big_endian );
    shentsize = get_u16( image+46, big_endian );
    shnum     = get_u16( image+48, big_endian );

  #ifdef ENABLE_DEBUG
    fprintf(stderr,"Imagelength : %lu\n", imagelen );
    fprintf(stderr,"PH at offset: %lu\n", phoff );
    fprintf(stderr,"PH itemlen  : %lu\n", phentsize );
    fprintf(stderr,"PH count    : %lu\n", phnum );
    fprintf(stderr,"SH at offset: %lu\n", shoff );
    fprintf(stderr,"SH itemlen  : %lu\n", shentsize );
    fprintf(stderr,"SH count    : %lu\n", shnum );
  #endif

    /* locate the note entry with the signature */
    if( phoff > imagelen )
	return SFSV_BAD_ELF; /* oops, does not fit into the image */
    for( ph = image + phoff; phnum ; phnum--, ph += phentsize ) {
	if( ph + phentsize > image+imagelen )
	    return SFSV_BAD_ELF; /* oops, does not fit into the image */
	if( get_u32(ph, big_endian) != PT_NOTE )
	    continue;
	off = get_u32(ph+4, big_endian );
	len = get_u32(ph+16, big_endian );
      #ifdef ENABLE_DEBUG
	fprintf(stderr,"note at %lu  len=%lu (phdr table)\n", off, len );
      #endif
	if( off > imagelen || len > imagelen || (off+len) > imagelen )
	    return SFSV_BAD_ELF; /* oops, does not fit into the image */
	rc = parse_note(image, off, len, big_endian, sigoff, siglen );
	if( rc != SFSV_NO_SIG )
	    return rc;
    }

    /* once more but this time look at the section table */
    if( shoff > imagelen )
	return SFSV_BAD_ELF; /* oops, does 8not fit into the image */
    for( sh = image + shoff; shnum ; shnum--, sh += shentsize ) {
	if( sh + shentsize > image+imagelen )
	    return SFSV_BAD_ELF; /* oops, does not fit into the image */
	if( get_u32(sh+4, big_endian) != SHT_NOTE )
	    continue;
	off = get_u32(sh+16, big_endian );
	len = get_u32(sh+20, big_endian );
      #ifdef ENABLE_DEBUG
	fprintf(stderr,"note at %lu  len=%lu (section table)\n", off, len );
      #endif
	if( off > imagelen || len > imagelen || (off+len) > imagelen )
	    return SFSV_BAD_ELF; /* oops, does not fit into the image */
	rc = parse_note(image, off, len, big_endian, sigoff, siglen );
	if( rc != SFSV_NO_SIG )
	    return rc;
    }

    return SFSV_NO_SIG;
}

#endif

