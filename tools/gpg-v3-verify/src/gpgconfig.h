/* config.h.  Generated automatically by configure.  */
/* config.h.in.  Generated automatically from configure.in by autoheader.  */
/* acconfig.h -  Small Footprint Signature Verification
 *	Copyright (C) 1999 Werner Koch (dd9jn).
 *
 * This file is part of SFSV.
 *
 * SFSV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SFSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */

#ifndef CONFIG_H
#define CONFIG_H



/* Define if you need to in order for stat and other things to work.  */
/* #undef _POSIX_SOURCE */

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1

/* #undef ENABLE_DEBUG */

/*#define BIG_ENDIAN_HOST 1 */
#define LITTLE_ENDIAN_HOST 1

/* #undef HAVE_BYTE_TYPEDEF */
#define HAVE_ULONG_TYPEDEF 1
/* #undef HAVE_U16_TYPEDEF */
/* #undef HAVE_U32_TYPEDEF */

/* The number of bytes in a unsigned int.  */
#define SIZEOF_UNSIGNED_INT 4

/* The number of bytes in a unsigned long.  */
#define SIZEOF_UNSIGNED_LONG 4

/* The number of bytes in a unsigned short.  */
#define SIZEOF_UNSIGNED_SHORT 2

/* Name of package */
#define PACKAGE "sfsv"

/* Version number of package */
#define VERSION "0.5.0"


#ifdef ENABLE_DEBUG
   #define NDEBUG 1
#endif

#ifdef __KERNEL__
#define NULL ((void *)0)
typedef int FILE;
typedef unsigned int size_t;
#endif


#endif /*CONFIG_H*/
