/* bigint.h
 *	Copyright (C) 1999 Werner Koch (dd9jn).
 *
 * This file is part of SFSV.
 *
 * SFSV is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SFSV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 */


#ifndef BIGINT_H
#define BIGINT_H

#include "types.h"

#if SIZEOF_UNSIGNED_INT == 4
   typedef unsigned int  LIMB;
   #define BYTES_PER_LIMB 4
   #define BITS_PER_LIMB  32
#elif SIZEOF_UNSIGNED_INT == 8
   typedef unsigned int  LIMB;
   #define BYTES_PER_LIMB 8
   #define BITS_PER_LIMB  64
#elif SIZEOF_UNSIGNED_INT == 2
   typedef unsigned int  LIMB;
   #define BYTES_PER_LIMB 2
   #define BITS_PER_LIMB  16
#else
   #error size of a limb is not known
#endif

#define BIGINT_MAX_LIMBS   136  /* 2048 bit plus a safty margin */
#define BIGINT_MAX_BITS    (BIGINT_MAX_LIMBS*BITS_PER_LIMB)


typedef struct {
    int allocated;
    int sign;
    int nlimbs;
    LIMB limbs[BIGINT_MAX_LIMBS];
} BIGINT;


/****************
 * Return the number of limbs used in A
 */
#define bi_nlimbs(a)  ((a)->nlimbs)


void bi_init( BIGINT *a, int nlimbs );
void bi_add( BIGINT *w, BIGINT *u, BIGINT *v );
void bi_sub( BIGINT *w, BIGINT *u, BIGINT *v );
void bi_mul( BIGINT *w, BIGINT *u, BIGINT *v );
void bi_mod( BIGINT *w, BIGINT *u, BIGINT *m );
void bi_div( BIGINT *w, BIGINT *u, BIGINT *m );

void bi_mulm( BIGINT *w, BIGINT *u, BIGINT *v, BIGINT *m );
void bi_invm( BIGINT *w, BIGINT *u, BIGINT *m );
void bi_expm( BIGINT *w, BIGINT *g, BIGINT *e, BIGINT *m );
void bi_mulexpm( BIGINT *w, BIGINT **g, BIGINT **e, int n, BIGINT *m );

int  bi_cmp( BIGINT *a, BIGINT *b );

void bi_print( FILE *fp, BIGINT *a );
int  bi_read( BIGINT *w, const byte *buf, size_t buflen );
int  bi_scan( BIGINT *w, const char *s );

#endif /*BIGINT_H*/
