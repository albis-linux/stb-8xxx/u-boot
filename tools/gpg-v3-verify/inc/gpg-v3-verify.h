/**
 * Copyright (C) Albis Technologies AG 2013-2013
 * All Rights Reserved
 * -OWNER-----------------------------------------------------------------------
 * Author      :
 *
 * -HISTORY---------------------------------------------------------------------
 *
 * -ISSUES---------------------------------------------------------------------_
 */

#ifndef _GPG_V3_VERIFY_H
#define _GPG_V3_VERIFY_H

#define __GPG_LIB_VERSION__ "1.0"

/* M.Schenk 2013.11.04, disabled for PBL*/
#ifdef __UBOOT_BUILD__
#include "initramfsHeader.h"
#endif

enum 
{
    SFSV_GOOD_SIG = 0,
    SFSV_BAD_SIG  = 1,
    SFSV_NO_KEY   = 2,
    SFSV_NO_SIG   = 3,
    SFSV_BAD_ELF  = 4,
    SFSV_BAD_PGP  = 5
};

enum
{
	SHA1_SIG_ID   = 2,
	SHA256_SIG_ID = 8
};

/**
 *  Verifying digital signature of image SHA version
 *  @param imageToVerify Pointer to beginning of image.
 *  @param imageToVerifySize Number of bytes of image.
 *  @param signature The digital signature created with the GNU Privacy Guard (gpg).
 *  @param signatureSize Number of bytes of the signature.
 *  @return See enum above!
 */
int verifyGpgV3Signature(char *imageToVerify,
                         unsigned int imageToVerifySize,
                         char *signature,
                         unsigned int signatureSize);

#ifdef __APPLET_BUILD__
#ifdef __PBL_USE_TKD_HASH__
/*=============================================================================
  1) Verify provided signature
  2) Send pre-calculated hash and signature to verification
  =============================================================================*/
int verifyGpgV3Hash(unsigned char *hash,char *signature,unsigned int signatureSize);

/*
 * Function for reading the Hash algorithm type of the signature
*/
int readHashID(const char *sigData, unsigned int sigSize, int *hashID);
#endif //__APPLET_BUILD__
#endif //__PBL_USE_TKD_HASH__

#if defined(__APPLICATION_BUILD__)
# 	define SERIAL_PRINTF 	printf
# 	define LABEL "[SWUP]  "
#elif defined(__UBOOT_BUILD__)
# 	define SERIAL_PRINTF 	serial_printf
# 	define LABEL "[UBOOT]  "
#elif defined(__APPLET_BUILD__)
# 	define SERIAL_PRINTF 	serial_printf
# 	define LABEL "[PBLAPPLET]  "
#elif defined(__PBL_BUILD__)
# 	define SERIAL_PRINTF 	__asc_xmit_printf
# 	define LABEL "[PBL]  "
extern int _pbl_albis_log_level;
extern int _pbl_albis_entry_level;
#else
#error "seems to be build outside PBL,Applet or Uboot"
#endif


/* debug macros */
#if defined(__SEC_LEVEL_STRICT__)
#	define LOGERROR(format, ...)  			{ }
#	define LOGINFO(format, ...)  			{ }
#	define LOGDEBUG(format, ...)  			{ }
#	define LOGTRACE(format, ...)  			{ }
#else
#	define LOGERROR(format, args...) 		do { SERIAL_PRINTF( LABEL format "\n" ,  ##args); } while(0)
#	define LOGINFO(format, args...) 		do { SERIAL_PRINTF( LABEL format "\n" ,  ##args); } while(0)
#	define LOGDEBUG(format, args...) 		do { if (_pbl_applet_log_level > 0) { SERIAL_PRINTF( LABEL format "\n" ,  ##args); } } while(0)
#	define LOGTRACE(format, args...) 		do { if (_pbl_applet_log_level > 1) { SERIAL_PRINTF( LABEL format "\n" ,  ##args); } } while(0)
#endif

#endif // _GPG_V3_VERIFY_H
