/*
 * (C) Copyright 2008-2013 ALBIS Technologies.
 *
 * Michael Schenk <michael.schenk@albistechnologies.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef _STB_8XXX_DRIVERS_H_
#define _STB_8XXX_DRIVERS_H_

#include <config.h>

/*----------------------------------------------------------------------
 * public functions declaration
 */
extern int stb_7105_force_recovery_boot_check(void);
extern int stb_7105_test_memory_check(void);
extern int stb_7105_board_init(void);
extern uchar stb_7105_get_hbi(void);

#define mdelay(n) 							({unsigned long msec=(n); while (msec--) udelay(1000);})

/**
 * Recovery boot check
 */	
#define RECOVER_BOOT_CHECK_TIME				5000	/* check for 5000 ms */
#define RECOVER_BOOT_CHECK_INTERVAL			200		/* check every 200 ms */

/**
 * memory test check
 */	
#define MEMORY_TEST_CHECK_TIME				5000	/* check for 5000 ms */
#define MEMORY_TEST_CHECK_INTERVAL			200		/* check every 200 ms */



/*----------------------------------------------------------------------
 * Audio Video Switch Visual Output STV6417/8/9 default values
 * according datasheet
 */

#define STV6419_I2C_ADDRESS 				0x4A

#define STV6419_REG_AUDIO0					0x00
#define STV6419_REG_AUDIO1					0x01
#define STV6419_REG_VIDEO0					0x02
#define STV6419_REG_VIDEO1					0x03
#define STV6419_REG_STANDBY					0x04

/*----------------------------------
 * encoder L/R selected
 * 0dB for normal audio inputs
 */ 
#define STV6419_REG_AUDIO0_VALUE			0x02

/*----------------------------------
 * soft volume change active
 * 0dB adjustment
 */ 
#define STV6419_REG_AUDIO1_VALUE			0x00

/*----------------------------------
 * bottom level clamp (R,G,B)
 * VCR bottom level clamp (R,G,B)
 * TV CVBS out enc selected
 * TV RGB out enc selected
 */ 
#define STV6419_REG_VIDEO0_VALUE			0x50

/*----------------------------------
 * slow blanking normal
 * SLV TV 16:9
 * VCR input only
 * fast blankinh high level (RGB/CVBS ????)
 * IT no interrupt flag
 */ 
#define STV6419_REG_VIDEO1_VALUE			0x24

/*----------------------------------
 * ENC inputs active
 * VCR inputs disabled
 * AUX inputs disabled
 * VCR outputs disabled
 * TV (CVBS) outputs ON
 * TV (RGB + FB) outputs ON
 * TV audio outputs ON
 */ 
#define STV6419_REG_STANDBY_VALUE			0x0e

/**
 * DVB-X tuner defines
 */	
#define DVB_RESET_TIME						100


#if (DISPLAY_VFD==2)
/**
 * VFD Controller HT16515 defines
 */	
// Timing
#define HT16515_TIMING_PWCLK				1 		// clock pulse with in us (>= 400ns)
#define HT16515_TIMING_CLKSTB			2 		// clock strobe time in us
#define HT16515_TIMING_PWSTB				2 		// strobe pulse width in us (>= 1us)
#define HT16515_RAM_SIZE					48		// internal display ram size in bytes (from  0x00 to 2F)

// Logic
#define HT16515_PIN_HIGH					1		// apply logic 1
#define HT16515_PIN_LOW					0		// apply logic 0

// Commands
#define HT16515_CMD1_INIT				0x0B	// Command 1: Display Mode Setting Command (default value)
												// b7-b6: =00 -> Command 1
												// b5-b4: not relevant (00)
												// b3-b0: =1011 -> 12 digits, 16 segments
#define HT16515_CMD2_INIT				0x40	// Command 2: Data Setting Command (default value)
												// b7-b6: =01 -> Command 2
												// b5-b4: not relevant (00)
												// b3: =0 -> normal operation mode
												// b2: =0 -> increment address after data has been written
												// b1-b0: =00 -> Write Data to Display Mode
#define HT16515_CMD3_INIT				0xc0	// Command 3: Address Setting Command (default value)
												// b7-b6: =11 -> Command 3
												// b5-b0: =00000 address=0x00 (0x00 to 0x2F)
#define HT16515_CMD4_INIT				0x8f	// Command 4: Display Control Command (default value)
												// b7-b6: =10 -> Command 4
												// b5-b4: not relevant (00)
												// b3: =1 -> Display ON
												// b2-b0: =111 -> Dimming Quantity: pulse width = 14/16
#endif


#endif
