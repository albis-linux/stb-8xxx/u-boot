/*
 * (C) Copyright 2008-2013 ALBIS Technologies.
 *
 * Michael Schenk <michael.schenk@albistechnologies.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <stm/stx7105reg.h>
#include <asm/io.h>
#include <stm/pio.h>
#include "stb_7105-i2c.h"

#ifdef DEBUG_I2C
DECLARE_GLOBAL_DATA_PTR;
#endif


/*-----------------------------------------------------------------------
 * Definitions
 */

#define RETRIES		0


#define I2C_ACK		0		/* PD_SDA level to ack a byte */
#define I2C_NOACK	1		/* PD_SDA level to noack a byte */


#ifdef DEBUG_I2C
#define PRINTD(fmt,args...)	do {	\
	if (gd->have_console)		\
		printf (fmt ,##args);	\
	} while (0)
#else
#define PRINTD(fmt,args...)
#endif

/*-----------------------------------------------------------------------
 * Local functions
 */
static void  stb_7105_send_reset	(void);
static void  stb_7105_send_start	(void);
static void  stb_7105_send_stop	(void);
static void  stb_7105_send_ack	(int);
static int   stb_7105_write_byte	(uchar byte);
static uchar stb_7105_read_byte	(int);


/*-----------------------------------------------------------------------
 * Send a reset sequence consisting of 9 clocks with the data signal high
 * to clock any confused device back into an idle state.  Also send a
 * <stop> at the end of the sequence for belts & suspenders.
 */
static void stb_7105_send_reset(void)
{
	int j;

	EXT_I2C_SCL(1);
	EXT_I2C_SDA(1);
#ifdef	EXT_I2C_INIT
	EXT_I2C_INIT;
#endif
	EXT_I2C_TRISTATE;
	for(j = 0; j < 9; j++) {
		EXT_I2C_SCL(0);
		EXT_I2C_DELAY;
		EXT_I2C_DELAY;
		EXT_I2C_SCL(1);
		EXT_I2C_DELAY;
		EXT_I2C_DELAY;
	}
	stb_7105_send_stop();
	EXT_I2C_TRISTATE;
}

/*-----------------------------------------------------------------------
 * START: High -> Low on SDA while SCL is High
 */
static void stb_7105_send_start(void)
{

	EXT_I2C_DELAY;
	EXT_I2C_SDA(1);
	EXT_I2C_ACTIVE;
	EXT_I2C_DELAY;
	EXT_I2C_SCL(1);
	EXT_I2C_DELAY;
	EXT_I2C_SDA(0);
	EXT_I2C_DELAY;
}

/*-----------------------------------------------------------------------
 * STOP: Low -> High on SDA while SCL is High
 */
static void stb_7105_send_stop(void)
{

	EXT_I2C_SCL(0);
	EXT_I2C_DELAY;
	EXT_I2C_SDA(0);
	EXT_I2C_ACTIVE;
	EXT_I2C_DELAY;
	EXT_I2C_SCL(1);
	EXT_I2C_DELAY;
	EXT_I2C_SDA(1);
	EXT_I2C_DELAY;
	EXT_I2C_TRISTATE;
}


/*-----------------------------------------------------------------------
 * ack should be I2C_ACK or I2C_NOACK
 */
static void stb_7105_send_ack(int ack)
{
	EXT_I2C_SCL(0);
	EXT_I2C_DELAY;
	EXT_I2C_ACTIVE;
	EXT_I2C_SDA(ack);
	EXT_I2C_DELAY;
	EXT_I2C_SCL(1);
	EXT_I2C_DELAY;
	EXT_I2C_DELAY;
	EXT_I2C_SCL(0);
	EXT_I2C_SDA(1);
	EXT_I2C_DELAY;
}


/*-----------------------------------------------------------------------
 * Send 8 bits and look for an acknowledgement.
 */
static int stb_7105_write_byte(uchar data)
{
	int j;
	int nack;

	EXT_I2C_ACTIVE;
	for(j = 0; j < 8; j++) {
		EXT_I2C_SCL(0);
		EXT_I2C_DELAY;
		EXT_I2C_SDA(data & 0x80);
		EXT_I2C_DELAY;
		EXT_I2C_SCL(1);
		EXT_I2C_DELAY;
		EXT_I2C_DELAY;

		data <<= 1;
	}

	/*
	 * Look for an <ACK>(negative logic) and return it.
	 */
	EXT_I2C_SCL(0);
	EXT_I2C_DELAY;
	EXT_I2C_SDA(1);
	EXT_I2C_TRISTATE;
	EXT_I2C_DELAY;
	EXT_I2C_SCL(1);
	EXT_I2C_DELAY;
	EXT_I2C_DELAY;
	nack = EXT_I2C_READ;
	EXT_I2C_SCL(0);
	EXT_I2C_DELAY;
	EXT_I2C_ACTIVE;

	return(nack);	/* not a nack is an ack */
}


/*-----------------------------------------------------------------------
 * if ack == I2C_ACK, ACK the byte so can continue reading, else
 * send I2C_NOACK to end the read.
 */
static uchar stb_7105_read_byte(int ack)
{
	int  data;
	int  j;

	/*
	 * Read 8 bits, MSB first.
	 */
	EXT_I2C_TRISTATE;
	data = 0;
	for(j = 0; j < 8; j++) {
		EXT_I2C_SCL(0);
		EXT_I2C_DELAY;
		EXT_I2C_DELAY;
		EXT_I2C_SCL(1);
		EXT_I2C_DELAY;
		data <<= 1;
		data |= EXT_I2C_READ;
		EXT_I2C_DELAY;
	}

	stb_7105_send_ack(ack);

	return(data);
}

/*=====================================================================*/
/*                         Public Functions                            */
/*=====================================================================*/

/*-----------------------------------------------------------------------
 * Initialization
 */
void stb_7105_i2c_init (int speed, int slaveaddr)
{
	/*
	 * WARNING: Do NOT save speed in a static variable: if the
	 * I2C routines are called before RAM is initialized (to read
	 * the DIMM SPD, for instance), RAM won't be usable and your
	 * system will crash.
	 */
	stb_7105_send_reset ();
}



/*-----------------------------------------------------------------------
 * Read bytes
 */
int  stb_7105_i2c_read(uchar chip, uint addr, int alen, uchar *buffer, int len)
{
	int shift;
	PRINTD("stb_7105_i2c_read: chip %02X addr %02X alen %d buffer %p len %d\n",
		chip, addr, alen, buffer, len);


	/*
	 * Do the addressing portion of a write cycle to set the
	 * chip's address pointer.  If the address length is zero,
	 * don't do the normal write cycle to set the address pointer,
	 * there is no address pointer in this chip.
	 */
	stb_7105_send_start();
	if(alen > 0) {
		if(stb_7105_write_byte(chip << 1)) {	/* write cycle */
			stb_7105_send_stop();
			PRINTD("stb_7105_i2c_read, no chip responded %02X\n", chip);
			return(1);
		}
		shift = (alen-1) * 8;
		while(alen-- > 0) {
			if(stb_7105_write_byte(addr >> shift)) {
				PRINTD("stb_7105_i2c_read, address not <ACK>ed\n");
				return(1);
			}
			shift -= 8;
		}
		stb_7105_send_stop();	/* reportedly some chips need a full stop */
		stb_7105_send_start();
	}
	/*
	 * Send the chip address again, this time for a read cycle.
	 * Then read the data.  On the last byte, we do a NACK instead
	 * of an ACK(len == 0) to terminate the read.
	 */
	stb_7105_write_byte((chip << 1) | 1);	/* read cycle */
	while(len-- > 0) {
		*buffer++ = stb_7105_read_byte(len == 0);
	}
	stb_7105_send_stop();
	return(0);
}

/*-----------------------------------------------------------------------
 * Write bytes
 */
int  stb_7105_i2c_write(uchar chip, uint addr, int alen, uchar *buffer, int len)
{
	int shift, failures = 0;

	PRINTD("stb_7105_i2c_write: chip %02X addr %02X alen %d buffer %p len %d\n",
		chip, addr, alen, buffer, len);

	stb_7105_send_start();
	if(stb_7105_write_byte(chip << 1)) {	/* write cycle */
		stb_7105_send_stop();
		PRINTD("stb_7105_i2c_write, no chip responded %02X\n", chip);
		return(1);
	}
	shift = (alen-1) * 8;
	while(alen-- > 0) {
		if(stb_7105_write_byte(addr >> shift)) {
			PRINTD("stb_7105_i2c_write, address not <ACK>ed\n");
			return(1);
		}
		shift -= 8;
	}

	while(len-- > 0) {
		if(stb_7105_write_byte(*buffer++)) {
			failures++;
		}
	}
	stb_7105_send_stop();
	return(failures);
}

/*-----------------------------------------------------------------------
 * Read a register
 */
uchar stb_7105_i2c_reg_read(uchar i2c_addr, uchar reg)
{
	uchar buf;

	stb_7105_i2c_read(i2c_addr, reg, 1, &buf, 1);

	return(buf);
}

/*-----------------------------------------------------------------------
 * Write a register
 */
void stb_7105_i2c_reg_write(uchar i2c_addr, uchar reg, uchar val)
{
	stb_7105_i2c_write(i2c_addr, reg, 1, &val, 1);
}



