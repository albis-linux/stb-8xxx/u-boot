/*
 * (C) Copyright 2008-2013 ALBIS Technologies.
 *
 * Michael Schenk <michael.schenk@albistechnologies.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <command.h>
#include <environment.h>
#include <config.h>
#include <nand.h>
#include <jffs2/jffs2.h>
#include <stm/soc.h>
#include <stm/stx7105reg.h>
#include <asm/io.h>
#include <stm/pio.h>
#include "stb_7105-drivers.h"
#include <i2c.h>

#ifdef __PROFILE_VERIMATRIX__

#include <gpg-v3-verify.h>
#include "../../../tools/libtomcrypt/src/headers/tomcrypt.h"
#include "aesXorKeyInner.h"

/* As the header(image_header_t) is aes-128 encrypted, make sure that the IMAGE_HEADER_SIZE
 * is a multiple of 16 (header is aes-128 encrypted) */
#define IMAGE_HEADER_SIZE (((sizeof(image_header_t)/AES_KEY_SIZE)+1)*AES_KEY_SIZE)

#endif

extern nand_info_t nand_info[];
extern flash_info_t flash_info[];	/* info for FLASH chips */

extern int memory_test(unsigned long *block_start, unsigned long block_length);
extern int memory_test_albis(unsigned long *block_start, unsigned long block_length);
extern int memory_test_stats_albis(void);

/* parition handling routines */
int mtdparts_init(void);
int find_dev_and_part(const char *id, struct mtd_device **dev,
		u8 *part_num, struct part_info **part);


/**
 * Enable NAND flash write
 *
 * @return none
 */
void flashWriteEnable(void)
{
	/* Disable WP for writing to NAND flash */
	STPIO_SET_PIN(PIO_PORT(1), 2, 1);
}

/**
 * Disable NAND flash write
 *
 * @return none
 */
void flashWriteDisable(void)
{
	/* keep NAND writeable */
}

/**
 * STB-7105 board init
 *
 * @return unused
 */
extern int board_init(void)
{
	stb_7105_board_init();

	return 0;
}

/**
 * Checking board
 *
 * @return unused
 */
int checkboard (void)
{
	printf ("\n\nBoard: STB7105"
#ifdef CONFIG_ST40_SE_MODE
			"  [32-bit mode]"
#else
			"  [29-bit mode]"
#endif
			"\n");

	return 0;
}


/**
 * Is called during the bootloader for a memory test of the SDRAM
 *
 * @return none
 */
void memoryTestSDRAM(void)
{
	ulong i;

	for (;;)
	{
#if defined(STB_8000) || defined(STB_8080) || defined(STB_8100)
		memory_test((unsigned long *) BLOCK_START_ADDRESS, (unsigned long) BLOCK_LENGTH);
#endif	  /* STB_8000, STB_8080, STB_8100 */

		printf("Type CTRL-C to Abort...");
		/* delay for PAUSE_TESTING ms... */
		for (i = 0; i < PAUSE_TESTING; i++)
		{
			udelay(1000);	/* 1 ms */
			/* check for ctrl-c to abort... */
			if (ctrlc()) {
				puts("Abort\n");
			}
		}
	}
}

#ifdef __PROFILE_VERIMATRIX__
/* =======================================================================================================================
 *  Calculate the SHA1 hash of the encrypted initramfs and compare it against the DSA signature in the header of initramfs
 * ======================================================================================================================= */

static int verify_signature(ulong addr)
{
	InitRamFsHeader_t *initRamFsHdr;
	char tmpbuf[IFS_HEADER_SIZE];
	int rc;

	memcpy(&tmpbuf, (char *)addr, IFS_HEADER_SIZE);
	initRamFsHdr = (InitRamFsHeader_t *)tmpbuf;
	/* Read the information from the header of the initramfs */
	initRamFsHdr->signatureSize=ntohl(initRamFsHdr->signatureSize);
	initRamFsHdr->imageSize=ntohl(initRamFsHdr->imageSize);

	/* Provide the header information and the start-address of the image for Signature Verification */
	rc = verifyGpgV3Signature((char*) (addr+IFS_HEADER_SIZE),
			initRamFsHdr->imageSize,
			initRamFsHdr->digitalSignature,
			initRamFsHdr->signatureSize);
	if( rc ) {
		printf("signature is NOT valid: %s\n",
				rc == SFSV_BAD_SIG ? "bad signature"
						: rc == SFSV_NO_KEY  ? "public key not available"
								: rc == SFSV_NO_SIG  ? "no signature found"
										: rc == SFSV_BAD_ELF ? "invalid elf object"
												: rc == SFSV_BAD_PGP ? "invalid signature packet"
														: "unknown reason" );
		return 1;
	}
	return 0;
}
#endif


/**
 * Calculate the AES key 2 by XORing it with the initialization vector (IV) stored in the bootloader
 * Decrypt the initramfs using the AES key 2 (16 bytes = 128 bits long)
 *
 * @param source_addr - The start address of the encrypted image
 * @param dest_addr   - address in SDRAM to which the decrypted image has to be copies
 * @param imageSize   - Size of the image
 * @return 0 valid, 1 invalid
 */
#ifdef __PROFILE_VERIMATRIX__
static int decrypt_image(ulong source_addr, ulong dest_addr, uint32_t imageSize)
{
	char key[AES_KEY_SIZE], IV[AES_KEY_SIZE];

	symmetric_CBC cbc;
	int err, i;

	/* Get the XORed AES Key and IV from aeskey.h
	   ========================================== */
	memcpy(key, hidden_inner_secret.key, AES_KEY_SIZE);
	memcpy(IV,  hidden_inner_secret.iv,  AES_KEY_SIZE);


	/* Remove the XORing on the secretkey using IV to obtain AES key 2
	   =============================================================== */
	for(i=0; i<AES_KEY_SIZE; i++) {
		key[i]= key[i] ^ IV[i];
	}

	/* Start to Decrypt the initramfs using AES Key 2
	   ============================================== */
	/* Initialize(Setup) the cipher
	   ============================ */

	/* register aes (Rijndael) */
	if (register_cipher(&aes_desc) == -1) {
		puts ("Error registering cipher \n");
		return 1;
	}

	if (( err = cbc_start(find_cipher("aes"), /* index of desired cipher */
			IV,      /* the initial vector to be used with the cipher */
			key,      /* the secret key */
			AES_KEY_SIZE,      /* length of secret key (16 bytes) */
			0,      /* Number of rounds in the cipher desired (0 for default) */
			&cbc)      /* The CBC state to initialize */
	) != CRYPT_OK) {
		puts (" cbc_encrypt error \n");
		return 1;
	}

	/* Set the cbc state to enable decryption
	   ====================================== */
	if ((err = cbc_setiv( IV, /* the initial IV we gave to cbc_start */
			AES_KEY_SIZE, /* the IV is 16 bytes long */
			&cbc)/* the cbc state we wish to modify */
	) != CRYPT_OK) {
		puts ("cbc_setiv error \n");
		return 1;
	}
#ifdef ENABLE_TIME_MEASUREMENT
	printf("\n ---> before starting to decrypt, after decrypt init : in msecs: %u\n", tick_to_time_albis(get_ticks()));
#endif

	/* Decrypt the Buffer in one step
	   ============================== */
	if (( err = cbc_decrypt( source_addr, 	/* ciphertext */
			dest_addr, 						/* [out] Plaintext  */
			imageSize, 						/* sizeof(buffer) - The number of bytes to process (must be multiple of block length) */
			&cbc) 							/* CBC state */
	) != CRYPT_OK) {
		puts (" cbc_decrypt error \n");
		return 1;
	}

	/* Terminate the Stream
	   ==================== */
	if ((err = cbc_done(&cbc)) != CRYPT_OK) {
		puts ("cbc_done error \n");
		return 1;
	}

	/* Clear Memory and Return
	   ======================= */
	zeromem(key, sizeof(key));
	zeromem(&cbc, sizeof(cbc));
	return 0;
}

#endif

/**
 * Checking a image (kernel) at a given address
 *
 * @param addr address in RAM to check
 * @return 0 valid, 1 invalid
 */
static int image_check(ulong addr)
{
	image_header_t header;
	ulong data;
	ulong len;
	ulong checksum;
	image_header_t *hdr = &header;

#ifdef __PROFILE_VERIMATRIX__
	InitRamFsHeader_t *initRamFsHdr;
	char tmpbuf[IFS_HEADER_SIZE];

	printf("## Verifiying Signature of the Image at %08lx using IFS Header at %08lx ... ", addr+IFS_HEADER_SIZE, addr);
	int result = verify_signature(addr);
	if (result) {
		puts ("\nSignature Check Failed!!!\n");
		return 1;
	} else {
		puts("Ok\n");
	}
#ifdef ENABLE_TIME_MEASUREMENT
	printf("\n ---> After Signature Check in msecs: %u\n", tick_to_time_albis(get_ticks()));
#endif
	printf("## Decrypting the Image from %08lx to %08lx ... ", addr+IFS_HEADER_SIZE, addr);
	memcpy(&tmpbuf, (char *)addr, IFS_HEADER_SIZE);
	initRamFsHdr = (InitRamFsHeader_t *)tmpbuf;

	/* Read the information from the header of the initramfs */
	initRamFsHdr->imageSize=ntohl(initRamFsHdr->imageSize);

	result = decrypt_image(addr+IFS_HEADER_SIZE, addr, initRamFsHdr->imageSize);
	if (result) {
		puts ("\nDecryption Failed!!!\n");
		return 1;
	} else {
		puts("Ok\n");
	}
#ifdef ENABLE_TIME_MEASUREMENT
	printf("\n ---> After Decryption in msecs: %u\n", tick_to_time_albis(get_ticks()));
#endif
#endif

	printf("## Checking Image at %08lx ... \n", addr);

	/* copy header so we can blank CRC field for re-calculation */
	memmove(&header, (char *)(addr), sizeof(image_header_t));

	if (ntohl(hdr->ih_magic) != IH_MAGIC) {
		puts ("   Bad Magic Number\n");
		return 1;
	} else {
		puts("## Verifying Magic Number... Ok\n");
	}

	data = (ulong)&header;
	len  = sizeof(image_header_t);

	checksum = ntohl(hdr->ih_hcrc);
	hdr->ih_hcrc = 0;

	puts("## Verifying Header Checksum ... ");

	if (crc32(0, (unsigned char *)data, len) != checksum) {
		puts ("   Bad Header Checksum\n");
		return 1;
	} else {
		puts("Ok\n");
	}

	/* for multi-file images we need the data part, too */
	image_print_contents ((image_header_t *)(addr));

	data = (addr) + sizeof(image_header_t);
	len  = ntohl(hdr->ih_size);

	puts("## Verifying Data Checksum ... ");
	if (crc32(0, (unsigned char *)data, len) != ntohl(hdr->ih_dcrc)) {
		puts ("   Bad Data CRC\n");
		return 1;
	} else {
		puts("Ok\n");
	}
#ifdef ENABLE_TIME_MEASUREMENT
	printf("\n ---> After crc in msecs: %u\n", tick_to_time_albis(get_ticks()));
#endif
	return 0;
}

/**
 * validate the u-boot environement area
 *
 * @return 0 valid, 1 invalid
 */
static int env_check(void)
{
	char hwsach[64];
	char mon_sector_def[64];
	int i;
	int size = 0;
	int fast_clk = 0;
	flash_info_t* info;

	puts("ENV:   ");

	/**
	 * hwsach=S1630-H8080-A-2
	 * fast clock for >= -20
	 */
	i = getenv_f ("hwsach", hwsach, sizeof (hwsach));
	if (i > 0) {
		const char* start_ptr = hwsach;
		const char* end_ptr = start_ptr + strlen(hwsach);
		unsigned int revision = 0;

		while(end_ptr > start_ptr) {
			if (*(end_ptr - 1) == '-') {
				revision = simple_strtoul(end_ptr, NULL, 10);
				break;
			}

			end_ptr--;
		}

		if (revision >= 20) {
			fast_clk = 1;
		}
	}

	/**
	 * Manual override some configuration options, which makes
	 * life easier when chaning some stuff.
	 */
	setenv("board",XSTR(BOARD) "_" XSTR(INPUT_CLOCK_RATE) "\0");
	setenv("monitor_base", XSTR(CONFIG_SYS_MONITOR_BASE) "\0");
	setenv("monitor_len", XSTR(CONFIG_SYS_MONITOR_LEN) "\0");
	setenv("load_addr", XSTR(CFG_LOAD_ADDR) "\0");
	setenv("monitor_sec", XSTR(MONITOR_SECTOR_DEFINITION) "\0");
	setenv("bootdelay", XSTR(CONFIG_BOOTDELAY) "\0");

	info = &flash_info[0];

	/* plug and play detection of monitor sectore definition */
	if (info->flash_id == FLASH_MAN_CFI) {
#if 0
		printf("FLASH info->sector_count [%d]\n", info->sector_count);
#endif
		for (i = 0; i < (info->sector_count - 1); ++i) {
#if 0
			printf(" [%d] start[i + 1] [0x%08x] start[i] [0x%08x] size [0x%08x]\n",
					i, info->start[i + 1], info->start[i], (info->start[i + 1] - info->start[i]));
#endif
			size += (info->start[i + 1] - info->start[i]);

			if (size >= CONFIG_SYS_MONITOR_LEN)
				break;
		}

		if (i < info->sector_count) {
			sprintf(mon_sector_def, "1:0-%d", i);
#if 0
			printf("probed mon_sector_def [%s]\n", mon_sector_def);
#endif
			setenv("monitor_sec", mon_sector_def);
		}
	}

	/* think about to have only a single bootm for main and recovery */

#if !defined(__SEC_LEVEL_STRICT__)
	setenv("execnfskernel", "dhcp " XSTR(KERNEL_DHCP_ADDRESS) "\0");
#endif
	setenv("execmainkernel", "bootm " XSTR(KERNEL_MAIN_ADDRESS) "\0");
	setenv("execrecoverykernel", "bootm " XSTR(KERNEL_RECOVERY_ADDRESS) "\0");

#if !defined(__SEC_LEVEL_STRICT__)
	if (fast_clk) {
		setenv("execnfsbootargs", "setenv bootargs console=ttyAS0,115200 nwhwconf=device:eth0,hwaddr:${ethaddr} root=/dev/nfs nfsroot=${serverip}:${rootpath},rsize=4096,wsize=8192,tcp ip=${ipaddr}:${serverip}:${gatewayip}:${netmask}:${hostname}::off bigphysarea=" XSTR(KERNEL_BIG_PHYS_AREA) " fastclk\0");
	}
	else {
		setenv("execnfsbootargs", "setenv bootargs console=ttyAS0,115200 nwhwconf=device:eth0,hwaddr:${ethaddr} root=/dev/nfs nfsroot=${serverip}:${rootpath},rsize=4096,wsize=8192,tcp ip=${ipaddr}:${serverip}:${gatewayip}:${netmask}:${hostname}::off bigphysarea=" XSTR(KERNEL_BIG_PHYS_AREA) "\0");
	}
#endif
#if defined(__PROFILE_VERIMATRIX__)
	if (fast_clk) {
		setenv("execmainbootargs",     "setenv bootargs console=ttyAS0,115200 nwhwconf=device:eth0,hwaddr:${ethaddr} root=/dev/initramfs bigphysarea=" XSTR(KERNEL_BIG_PHYS_AREA) " fastclk\0");
		setenv("execrecoverybootargs", "setenv bootargs console=ttyAS0,115200 nwhwconf=device:eth0,hwaddr:${ethaddr} root=/dev/initramfs bigphysarea=" XSTR(KERNEL_BIG_PHYS_AREA) " fastclk\0");
	}
	else {
		setenv("execmainbootargs",     "setenv bootargs console=ttyAS0,115200 nwhwconf=device:eth0,hwaddr:${ethaddr} root=/dev/initramfs bigphysarea=" XSTR(KERNEL_BIG_PHYS_AREA) "\0");
		setenv("execrecoverybootargs", "setenv bootargs console=ttyAS0,115200 nwhwconf=device:eth0,hwaddr:${ethaddr} root=/dev/initramfs bigphysarea=" XSTR(KERNEL_BIG_PHYS_AREA) "\0");
	}
#else
	if (fast_clk) {
		setenv("execmainbootargs", "setenv bootargs console=ttyAS0,115200 nwhwconf=device:eth0,hwaddr:${ethaddr} " XSTR(MAIN_ROOT_UBI_MTD)" root=" XSTR(MAIN_ROOT_DEVICE) " rootfstype=" XSTR(MAIN_ROOT_DEVICE_FS) " bigphysarea="  XSTR(KERNEL_BIG_PHYS_AREA) " fastclk\0");
		setenv("execrecoverybootargs", "setenv bootargs console=ttyAS0,115200 nwhwconf=device:eth0,hwaddr:${ethaddr} root=/dev/initramfs bigphysarea=" XSTR(KERNEL_BIG_PHYS_AREA) " fastclk\0");
	}
	else {
		setenv("execmainbootargs", "setenv bootargs console=ttyAS0,115200 nwhwconf=device:eth0,hwaddr:${ethaddr} " XSTR(MAIN_ROOT_UBI_MTD)" root=" XSTR(MAIN_ROOT_DEVICE) " rootfstype=" XSTR(MAIN_ROOT_DEVICE_FS) " bigphysarea="  XSTR(KERNEL_BIG_PHYS_AREA) "\0");
		setenv("execrecoverybootargs", "setenv bootargs console=ttyAS0,115200 nwhwconf=device:eth0,hwaddr:${ethaddr} root=/dev/initramfs bigphysarea=" XSTR(KERNEL_BIG_PHYS_AREA) "\0");
	}
#endif

	/**
	 * configuration eeprom definition
	 */
	setenv("cfg_offset", XSTR(CONFIG_ENV_OFFSET) "\0");
	setenv("cfg_size", XSTR(CONFIG_ENV_SIZE) "\0");

	/**
	 * due to place kernel in NAND we need some special
	 * cmd to manual load main or recover. The kernel
	 * must first loaded from NAND to memory.
	 */
	setenv("execforcemainboot", "nboot.jffs2 " XSTR(KERNEL_MAIN_ADDRESS) " " XSTR(KERNEL_MAIN_MTD_PART)"; run execmainbootargs; run execmainkernel\0");
	setenv("execforcerecoveryboot", "nboot.jffs2 " XSTR(KERNEL_RECOVERY_ADDRESS) " " XSTR(KERNEL_RECOVERY_MTD_PART)"; run execrecoverybootargs; run execrecoverykernel\0");

	/**
	 * for using MTD parts inside u-boot we first need to load
	 * the defaults.
	 */
	setenv("mtdids", MTDIDS_DEFAULT);
	setenv("mtdparts", MTDPARTS_DEFAULT);
	setenv("partition", NULL);

	mtdparts_init();

	/**
	 * verify u-boot configuration area
	 */
	{
		ulong crc, len, new;
		unsigned off;
		uchar buf[64];

		/* this will call i2c_init (CFG_I2C_SPEED, CFG_I2C_SLAVE);*/
		eeprom_init ();	/* prepare for EEPROM read/write */

		/* read old CRC */
		eeprom_read (CONFIG_SYS_DEF_EEPROM_ADDR,
					 CONFIG_ENV_OFFSET+offsetof(env_t,crc),
					 (uchar *)&crc, sizeof(ulong));

		new = 0;
		len = ENV_SIZE;
		off = offsetof(env_t,data);
		while (len > 0) {
			int n = (len > sizeof(buf)) ? sizeof(buf) : len;

			eeprom_read (CONFIG_SYS_DEF_EEPROM_ADDR, CONFIG_ENV_OFFSET+off, buf, n);
			new = crc32 (new, buf, n);
			len -= n;
			off += n;
		}

		if (crc == new) {
			puts("is valid\n");
		} else {
			puts("is bad\n");
			return (1);
		}
	}

	return (0);
}

/**
 * Load a kernel from a given partition to a given address.
 *
 * @param partition string describing device and partition or partition name
 * @param addr address in RAM to which kernel will be loaded
 * @return 0 on success, 1 otherwise
 */
int load_kernel_from_nand(char* partition, ulong addr)
{
	int r;
	size_t cnt;
	u8 pnum;
	loff_t offset = 0;
	image_header_t* hdr;
	nand_info_t* nand;
	struct mtd_device* dev;
	struct part_info* part;
#ifdef __PROFILE_VERIMATRIX__
	InitRamFsHeader_t* initRamFsHdr;
	char tmpbuf_image_header_t[IMAGE_HEADER_SIZE];
	char tmpbuf_InitRamFsHeader[IFS_HEADER_SIZE];
#endif

#ifdef ENABLE_TIME_MEASUREMENT
	printf("\n ---> Before Copying to RAM in msecs: %u\n", tick_to_time_albis(get_ticks()));
#endif
	if (mtdparts_init() != 0) {
		puts("mtdparts_init error\n");
		return 1;
	}

	if (find_dev_and_part(partition, &dev, &pnum, &part) != 0) {
		puts("find_dev_and_part error\n");
		return 1;
	}

	if (dev->id->type != MTD_DEV_TYPE_NAND) {
		puts("not a NAND\n");
		return 1;
	}

	nand = &nand_info[dev->id->num];
	offset = part->offset;

	printf("Loading from %s, offset 0x%08x\n", nand->name, (unsigned int)offset);

#ifdef __PROFILE_VERIMATRIX__
	cnt = IFS_HEADER_SIZE + IMAGE_HEADER_SIZE;
	r = nand_read_skip_bad(nand, offset, &cnt, (u_char *)addr);
#else
	cnt = sizeof (image_header_t);
	r = nand_read_skip_bad(nand, offset, &cnt, (u_char *)addr);
#endif

	if (r) {
		puts("nand_read_opts error\n");
		return 1;
	}

#ifdef __PROFILE_VERIMATRIX__
	memcpy(&tmpbuf_image_header_t, (char *)(addr+IFS_HEADER_SIZE), IMAGE_HEADER_SIZE);
	printf("## Decrypting Header to Verify Magic Number... ");
	if (decrypt_image(tmpbuf_image_header_t, tmpbuf_image_header_t, IMAGE_HEADER_SIZE) != 0) {
		return 1;
	} else {
		printf("Ok\n");
	}
	hdr = (image_header_t *) tmpbuf_image_header_t;
#ifdef ENABLE_TIME_MEASUREMENT
	printf("\n ---> After Decrypting the Header in msecs: %u\n", tick_to_time_albis(get_ticks()));
#endif
#else
	hdr = (image_header_t *) addr;
#endif

	printf("## Verifying Magic Number... ");
	if (ntohl(hdr->ih_magic) != IH_MAGIC) {
		printf("\n** Bad Magic Number 0x%x **\n", hdr->ih_magic);
		return 1;
	} else {
		printf("Ok\n");
	}

	image_print_contents(hdr);

#ifdef __PROFILE_VERIMATRIX__
	/* Read the information from the header of the initramfs */
	memcpy(&tmpbuf_InitRamFsHeader, (char *)(addr), IFS_HEADER_SIZE);
	initRamFsHdr = (InitRamFsHeader_t *)tmpbuf_InitRamFsHeader;

	cnt = ntohl(initRamFsHdr->imageSize) + IFS_HEADER_SIZE;
#else
	cnt = (ntohl(hdr->ih_size) + sizeof (image_header_t));
#endif
	r = nand_read_skip_bad(nand, offset, &cnt, (u_char *)addr);

	if (r) {
		puts("nand_read_opts error\n");
		return 1;
	}

	/* Loading ok, update default load address */
	load_addr = addr;
	printf("## Loading from NAND to RAM... Ok\n");
#ifdef ENABLE_TIME_MEASUREMENT
	printf("\n ---> After copying to RAM in msecs: %u\n", tick_to_time_albis(get_ticks()));
#endif
	return 0;
}

void set_led_state(int state_val, int delay_in_ms) {
	int toggle = 0, abort = 0;

	switch(state_val) {
	case 0: // toggle_red_blue_forever
		STPIO_SET_PIN(PIO_PORT(5), 2, 0);		/* Blue LED off */
		STPIO_SET_PIN(PIO_PORT(5), 3, 1);       /* Red LED on   */
		while(!abort) {
			mdelay(delay_in_ms);
			/* toggle between Red and Blue */
			STPIO_SET_PIN(PIO_PORT(5), 3, toggle);
			toggle = !toggle;
			STPIO_SET_PIN(PIO_PORT(5), 2, toggle);
		}
		break;
	case 1: // toggle_red_forever
		STPIO_SET_PIN(PIO_PORT(5), 2, 0);		/* Blue LED off */
		STPIO_SET_PIN(PIO_PORT(5), 3, 1);       /* Red LED on   */
		while(!abort) {
			mdelay(delay_in_ms);
			/* toggle on Red */
			toggle = !toggle;
			STPIO_SET_PIN(PIO_PORT(5), 3, toggle);
		}
		break;
	default:
		break;
	}
}

/**
 * Misc init functions (image validation)
 *
 * @return 0 on success, 1 otherwise
 */
int misc_init_r (void)
{
	char* value;
	int abort = 0;

#if defined(STB_8500)
	/* check if memory test has to be done */
	if (stb_7105_test_memory_check() == 1) {
		/*** GIVE A FEEDBACK TO THE USER THAT MEMORY TEST WILL START ***/

		puts("\n\nSDRAM memory test will started now...\n");

		/* Set STANDBY_LED to red & blue! */
		STPIO_SET_PIN(PIO_PORT(5), 2, 1);
		STPIO_SET_PIN(PIO_PORT(5), 3, 1);

		/*** START MEMORY TEST ***/
		memoryTestSDRAM();

		return 0;
	}
#endif	  /* STB_8500 */

/**
 * M.Schenk 2013.01.21
 * if env is not valid in case of factory, write the default values.
 */
#ifndef __ENV_SAVE_DEFAULTS__
	/* check if we have a valid env */
	if (env_check() != 0) {
#if defined(__PROFILE_VERIMATRIX__) && defined(__SEC_LEVEL_STRICT__)
		/* set LED to toggle on Red */
		set_led_state(1, 1000);
#else
		int toggle = 0;
		puts("!!! no valid environment found!\n");
		STPIO_SET_PIN(PIO_PORT(5), 2, 0);		/* blue LED off */
		while(!abort) {
			int i;

			for(i=0; !abort && i<100; ++i) 	{
				/* Do we have got a key press? */
				if (tstc()) {
					abort = 1;
					setenv("bootcmd", " ");
					(void) getc();  /* consume input	*/
					STPIO_SET_PIN(PIO_PORT(5), 3, 1);	/* red LED on */
					return 0;
				}
				mdelay(10);		/* wait for 10 ms */
			}

			/* let blink the red LED */
			toggle = !toggle;
			STPIO_SET_PIN(PIO_PORT(5), 3, toggle);
			/* mdelay(1000); */
		}
#endif
	}
#else
	/* check if we have a valid env */
	if (env_check() != 0) {
		puts("!!! no valid environment found, write default values\n");
		mdelay(500);
		saveenv();
		puts("!!! default values written\n");
	}
#endif

	/* do no image check when booting over nfs */
	if (((value = getenv((char*)"bootcmd")) != NULL) && (strcmp(value,"run execnfsboot") == 0)) {
		puts ("NFS booting active, skipping image check\n");
		return 0;
	}

	/* force recovery boot check */
	if (stb_7105_force_recovery_boot_check() == 1) {
		puts("Load recovery kernel from NAND to RAM\n");

		if(load_kernel_from_nand(KERNEL_RECOVERY_MTD_PART, KERNEL_RECOVERY_ADDRESS) != 0) {
			puts("loading recovery kernel failed\n");
		}

		puts("Force recovery boot\n");

		/* check recovery image */
		if(image_check(KERNEL_RECOVERY_ADDRESS) == 0) {
			setenv("bootcmd", "run execrecoveryboot");
			return 0;
		}

		puts("No valid recovery image\n");
	}


	/* load main kernel from NAND to RAM */
	puts("\nLoad main kernel from NAND to RAM :\n");

	if (load_kernel_from_nand(KERNEL_MAIN_MTD_PART, KERNEL_MAIN_ADDRESS) != 0) {
		puts("loading kernel failed\n");
	}

	puts("\nChecking main image :\n");

	/* check main image */
	if (image_check(KERNEL_MAIN_ADDRESS) == 0) {
		puts("\nFound valid main image\n\n");
		return 0;
	}

	/* load recovery kernel from NAND to RAM */
	puts("Load recovery kernel from NAND to RAM\n");

	if (load_kernel_from_nand(KERNEL_RECOVERY_MTD_PART, KERNEL_RECOVERY_ADDRESS) != 0) {
		puts("loading kernel failed\n");
	}

	puts("Checking recovery image\n");

	/* check recovery image */
	if (image_check(KERNEL_RECOVERY_ADDRESS) == 0) {
		puts("!!! booting from recovery image !!!\n");
		setenv("bootcmd", "run execrecoveryboot");
		return 0;
	}

#if defined(__SEC_LEVEL_STRICT__)
	/* in strict mode no NFS functionality available. That means STB is end of life!! */
	puts("!!!! no valid images found, Box is dead !!!!!\n");
#ifdef __PROFILE_VERIMATRIX__
	/* set LED to toggle between Red and Blue */
	set_led_state(0, 1000);
#else
	/* sleep 30 seconds */
	mdelay(30000);
#endif
#else
	puts("no valid images found, force NFS booting\n");

	/*  both images are bad and bootcmd is not nfs, so try the best and activate nfs booting  */
	setenv("bootcmd", "run execnfsboot");
#endif

	return 0;
}

/**
 * Board video init function for splashscreen support
 *
 * @return 0 on success, 1 otherwise
 */
unsigned int board_video_init(void)
{
	return 0;
}

/**
 * Special reset cmd function
 *
 */
void do_reset (cmd_tbl_t * cmdtp, bd_t * bd, int flag, int argc, char *argv[])
{
	/**
	 * HW problem on some PCBs on which POWER_ON_ETH takes to much time
	 * for going to low level during reset. So we force it to low just
	 * before we initiate a software reset.
	 */

	/* Put POWER_ON_ETH to low level */
	STPIO_SET_PIN(PIO_PORT(15), 5, 0);

	/* Configure POWER_ON_ETH PIO as output */
	SET_PIO_PIN(PIO_PORT(15), 5, STPIO_OUT);

	/**
	 * end of POWER_ON_ETH WA.
	 */

	/* workaround for buggy reset */
	SET_PIO_PIN(PIO_PORT(5), 0, STPIO_OUT);
	STPIO_SET_PIN(PIO_PORT(5), 0, 0);

	/* on correct reset circuite, the reset kicks in here */

	udelay(100);

	STPIO_SET_PIN(PIO_PORT(5), 0, 1);

	/* wait for H/W reset to kick in ... */
	for (;;);
}

