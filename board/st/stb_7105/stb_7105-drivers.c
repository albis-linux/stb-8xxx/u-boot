/*
 * (C) Copyright 2008-2013 ALBIS Technologies.
 *
 * Michael Schenk <michael.schenk@albistechnologies.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <command.h>
#include <stm/soc.h>
#include <stm/stx7105reg.h>
#include <asm/io.h>
#include <stm/pio.h>
#include <i2c.h>
#include "stb_7105-i2c.h"
#include "stb_7105-drivers.h"

#if defined(CONFIG_SOFT_I2C)
extern void stx7105_i2c_scl(const int val)
{
#if defined(CONFIG_I2C_BUS_A)			/* Use I2C Bus "A" */
	STPIO_SET_PIN(PIO_PORT(2), 2, (val) ? 1 : 0);
#elif defined(CONFIG_I2C_BUS_C)			/* Use I2C Bus "C" */
	STPIO_SET_PIN(PIO_PORT(3), 4, (val) ? 1 : 0);
#elif defined(CONFIG_I2C_BUS_D)			/* Use I2C Bus "D" */
	STPIO_SET_PIN(PIO_PORT(3), 6, (val) ? 1 : 0);
#endif
}

extern void stx7105_i2c_sda(const int val)
{
#if defined(CONFIG_I2C_BUS_A)			/* Use I2C Bus "A" */
	STPIO_SET_PIN(PIO_PORT(2), 3, (val) ? 1 : 0);
#elif defined(CONFIG_I2C_BUS_C)			/* Use I2C Bus "C" */
	STPIO_SET_PIN(PIO_PORT(3), 5, (val) ? 1 : 0);
#elif defined(CONFIG_I2C_BUS_D)			/* Use I2C Bus "D" */
	STPIO_SET_PIN(PIO_PORT(3), 7, (val) ? 1 : 0);
#endif
}

extern int stx7105_i2c_read(void)
{
#if defined(CONFIG_I2C_BUS_A)			/* Use I2C Bus "A" */
	return STPIO_GET_PIN(PIO_PORT(2), 3);
#elif defined(CONFIG_I2C_BUS_C)			/* Use I2C Bus "C" */
	return STPIO_GET_PIN(PIO_PORT(3), 5);
#elif defined(CONFIG_I2C_BUS_D)			/* Use I2C Bus "D" */
	return STPIO_GET_PIN(PIO_PORT(3), 7);
#endif
}
#endif	/* CONFIG_SOFT_I2C */

#if defined(CONFIG_I2C_CMD_TREE)
extern unsigned int i2c_get_bus_speed(void)
{
	return CONFIG_SYS_I2C_SPEED;
}
extern int i2c_set_bus_speed(unsigned int speed)
{
	return -1;
}
#endif	/* CONFIG_I2C_CMD_TREE */

/**
 * check recovery force booting switch (standby switch)
 *
 * @return 0 not pressed, 1 presses
 */
int stb_7105_force_recovery_boot_check(void)
{
	int cnt;

	if (STPIO_GET_PIN(PIO_PORT(2), 1) != 0)
		return 0;

	cnt = RECOVER_BOOT_CHECK_TIME / RECOVER_BOOT_CHECK_INTERVAL;

	while (cnt-- != 0) {
		mdelay(RECOVER_BOOT_CHECK_INTERVAL);
		if (STPIO_GET_PIN(PIO_PORT(2), 1) != 0)
			return 0;
	}

	return 1;
}

/**
 * check if memory test has to be done
 *
 * @return 0 = OK button not pressed, 1 = OK button pressed
 */
int stb_7105_test_memory_check(void)
{
	int cnt;

	/* check if KEY_OK button is pressed */
	if (STPIO_GET_PIN(PIO_PORT(11), 6) != 0)
		return 0;

	cnt = MEMORY_TEST_CHECK_TIME / MEMORY_TEST_CHECK_INTERVAL;

	while (cnt-- != 0) {
		mdelay(MEMORY_TEST_CHECK_INTERVAL);
		/* check if KEY_OK button is pressed */
		if (STPIO_GET_PIN(PIO_PORT(11), 6) != 0)
			return 0;
	}

	return 1;
}

/**
 * Init function for Audio/Video Switch Chip STV6419
 *
 * @return none
 */
static void stb_7105_stv6419_init(void)
{
	/*
	 * init i2c Bus
	 */
	stb_7105_i2c_init(EXT_CONFIG_SYS_I2C_SPEED, EXT_CFG_I2C_SLAVE);

	/*
	 * set init valus of the STV6419 Registers
	 */
	stb_7105_i2c_reg_write(STV6419_I2C_ADDRESS, STV6419_REG_AUDIO0,
						   STV6419_REG_AUDIO0_VALUE);
	stb_7105_i2c_reg_write(STV6419_I2C_ADDRESS, STV6419_REG_AUDIO1,
						   STV6419_REG_AUDIO1_VALUE);
	stb_7105_i2c_reg_write(STV6419_I2C_ADDRESS, STV6419_REG_VIDEO0,
						   STV6419_REG_VIDEO0_VALUE);
	stb_7105_i2c_reg_write(STV6419_I2C_ADDRESS, STV6419_REG_VIDEO1,
						   STV6419_REG_VIDEO1_VALUE);
	stb_7105_i2c_reg_write(STV6419_I2C_ADDRESS, STV6419_REG_STANDBY,
						   STV6419_REG_STANDBY_VALUE);
}

/**
 * STB-7105 ethernet configuration
 *
 * @return none
 */
static void stb_7105_config_ethernet(void)
{
	unsigned long sysconf;

	/* Configure & Reset the Ethernet PHY */

	/* Set the GMAC in MII mode */
	sysconf = *STX7105_SYSCONF_SYS_CFG07;
	sysconf &= ~0x060f0000ul;
	sysconf |=  0x08010000ul;
	*STX7105_SYSCONF_SYS_CFG07 = sysconf;

	sysconf = *STX7105_SYSCONF_SYS_CFG37;
	/* PIO7[4] CFG37[12,4]  AltFunction = 1 */
	/* PIO7[5] CFG37[13,5]  AltFunction = 1 */
	/* PIO7[6] CFG37[14,6]  AltFunction = 1 */
	/* PIO7[7] CFG37[15,7]  AltFunction = 1 */
	sysconf &= ~0xf0f0ul;	/* 3,3,3,3,0,0,0,0 */
	*STX7105_SYSCONF_SYS_CFG37 = sysconf;

	sysconf = *STX7105_SYSCONF_SYS_CFG46;
	/* PIO8[0] CFG46[8,0]   AltFunction = 1 */
	/* PIO8[1] CFG46[9,1]   AltFunction = 1 */
	/* PIO8[2] CFG46[10,2]  AltFunction = 1 */
	/* PIO8[3] CFG46[11,3]  AltFunction = 1 */
	/* PIO8[4] CFG46[12,4]  AltFunction = 1 */
	/* PIO8[5] CFG46[13,5]  AltFunction = 1 */
	/* PIO8[6] CFG46[14,6]  AltFunction = 1 */
	/* PIO8[7] CFG46[15,7]  AltFunction = 1 */
	sysconf &= ~0xfffful;	/* 3,3,3,3,3,3,3,3 */
	*STX7105_SYSCONF_SYS_CFG46 = sysconf;

	sysconf = *STX7105_SYSCONF_SYS_CFG47;
	/* PIO9[0] CFG47[8,0]   AltFunction = 1 */
	/* PIO9[1] CFG47[9,1]   AltFunction = 1 */
	/* PIO9[2] CFG47[10,2]  AltFunction = 1 */
	/* PIO9[3] CFG47[11,3]  AltFunction = 1 */
	/* PIO9[4] CFG47[12,4]  AltFunction = 1 */
	/* PIO9[5] CFG47[13,5]  AltFunction = 1 */
	/* PIO9[6] CFG47[14,6]  AltFunction = 1 */
	sysconf &= ~0x7f7ful;	/* 0,3,3,3,3,3,3,3 */
	*STX7105_SYSCONF_SYS_CFG47 = sysconf;

	/* Setup PIO for the Ethernet's MII bus */
	SET_PIO_PIN(PIO_PORT(7),4,STPIO_IN);
	SET_PIO_PIN(PIO_PORT(7),5,STPIO_IN);
	SET_PIO_PIN(PIO_PORT(7),6,STPIO_ALT_OUT);
	SET_PIO_PIN(PIO_PORT(7),7,STPIO_ALT_OUT);
	SET_PIO_PIN(PIO_PORT(8),0,STPIO_ALT_OUT);
	SET_PIO_PIN(PIO_PORT(8),1,STPIO_ALT_OUT);
	SET_PIO_PIN(PIO_PORT(8),2,STPIO_ALT_OUT);
	SET_PIO_PIN(PIO_PORT(8),3,STPIO_ALT_BIDIR);
	SET_PIO_PIN(PIO_PORT(8),4,STPIO_ALT_OUT);
	SET_PIO_PIN(PIO_PORT(8),5,STPIO_IN);
	SET_PIO_PIN(PIO_PORT(8),6,STPIO_IN);
	SET_PIO_PIN(PIO_PORT(8),7,STPIO_IN);
	SET_PIO_PIN(PIO_PORT(9),0,STPIO_IN);
	SET_PIO_PIN(PIO_PORT(9),1,STPIO_IN);
	SET_PIO_PIN(PIO_PORT(9),2,STPIO_IN);
	SET_PIO_PIN(PIO_PORT(9),3,STPIO_IN);
	SET_PIO_PIN(PIO_PORT(9),4,STPIO_IN);
	SET_PIO_PIN(PIO_PORT(9),5,STPIO_ALT_OUT);
	SET_PIO_PIN(PIO_PORT(9),6,STPIO_IN);

	/* Setup PIO for the PHY's reset */
	SET_PIO_PIN(PIO_PORT(15), 5, STPIO_OUT);

	/* Hard Reset the PHY -- do after we have configured the MAC */
	STPIO_SET_PIN(PIO_PORT(15), 5, 0);
	udelay(100);	/* small delay */
	STPIO_SET_PIN(PIO_PORT(15), 5, 1);
}

/**
 * return value of hardware boot identifier
 *
 * @return hbi value
 */
uchar stb_7105_get_hbi(void)
{
	uchar hbi_value = 0;

	/**
	 * Hardware boot identifier (HBI)
	 * ------------------------------
	 * HBI1 is on 1/5 (input)
	 * HBI2 is on 1/4 (input)
	 * HBI3 is on 1/3 (input)
	 */
	SET_PIO_PIN(PIO_PORT(1), 5, STPIO_IN);
	SET_PIO_PIN(PIO_PORT(1), 4, STPIO_IN);
	SET_PIO_PIN(PIO_PORT(1), 3, STPIO_IN);

	hbi_value = ((STPIO_GET_PIN(PIO_PORT(1), 3) << 2) | (STPIO_GET_PIN(PIO_PORT(1), 4) << 1) | STPIO_GET_PIN(PIO_PORT(1), 5));

	return hbi_value;
}

/**
 * STB-7105 smart card configuration
 *
 * @return none
 */
static void stb_7105_config_smart_card(void)
{
	/**
	 * Smart card configuration
	 * ------------------------
	 * SC0DATAOUT_1 is on 0/0 (bidirectional)
	 * SC0DATAOUT_1 is on 0/1 (input)
	 * SC0EXTCLK_1 is on 0/2 (input)
	 * SC0CLK_1 is on 0/3 (output)
	 * SC0RESET_1 is on 0/4 (output)
	 * SC0CMDVCC_1 is on 0/5 (output)
	 * 5V3V_1 is on 0/6 (output)
	 * SC0DET_1 is on 0/7 (input)
	 */
	SET_PIO_PIN(PIO_PORT(0), 0, STPIO_BIDIR);
	SET_PIO_PIN(PIO_PORT(0), 1, STPIO_IN);
	SET_PIO_PIN(PIO_PORT(0), 2, STPIO_IN);
	SET_PIO_PIN(PIO_PORT(0), 3, STPIO_OUT);
	SET_PIO_PIN(PIO_PORT(0), 4, STPIO_OUT);
	SET_PIO_PIN(PIO_PORT(0), 5, STPIO_OUT);
	SET_PIO_PIN(PIO_PORT(0), 6, STPIO_OUT);
	SET_PIO_PIN(PIO_PORT(0), 7, STPIO_IN);
}

/**
 * STB-7105 I2C configuration
 *
 * @return none
 */
static void stb_7105_config_i2c_busses(void)
{
	/**
	 * I2C bus A for HDMI
	 * ------------------------
	 * I2C_SCLA is on 2/2 (output, high level)
	 * I2C_SDAA is on 2/3 (input, high level)
	 */
	STPIO_SET_PIN(PIO_PORT(2), 2, 1);
	STPIO_SET_PIN(PIO_PORT(2), 3, 1);
	SET_PIO_PIN(PIO_PORT(2), 2, STPIO_OUT);
	SET_PIO_PIN(PIO_PORT(2), 3, STPIO_IN);


	/**
	 * I2C bus B for TUNER1/TUNER2
	 * ---------------------------
	 * I2C_SCLB is on 2/5 (output, high level)
	 * I2C_SDAB is on 2/6 (input, high level)
	 */
	STPIO_SET_PIN(PIO_PORT(2), 5, 1);
	STPIO_SET_PIN(PIO_PORT(2), 6, 1);
	SET_PIO_PIN(PIO_PORT(2), 5, STPIO_OUT);
	SET_PIO_PIN(PIO_PORT(2), 6, STPIO_IN);


	/**
	 * I2C bus C for EEPROM
	 * --------------------
	 * I2C_SCLC is on 3/4 (output, high level)
	 * I2C_SDAC is on 3/5 (input, high level)
	 */
	STPIO_SET_PIN(PIO_PORT(3), 4, 1);
	STPIO_SET_PIN(PIO_PORT(3), 5, 1);
	SET_PIO_PIN(PIO_PORT(3), 4, STPIO_BIDIR);
	SET_PIO_PIN(PIO_PORT(3), 5, STPIO_BIDIR);


	/**
	 * I2C bus D for SCART SWITCH / CI
	 * -------------------------------
	 * I2C_SCLD is on 3/6 (output, high level)
	 * I2C_SDAD is on 3/7 (input, high level)
	 */
	STPIO_SET_PIN(PIO_PORT(3), 6, 1);
	STPIO_SET_PIN(PIO_PORT(3), 7, 1);
	SET_PIO_PIN(PIO_PORT(3), 6, STPIO_BIDIR);
	SET_PIO_PIN(PIO_PORT(3), 7, STPIO_BIDIR);

}

/**
 * STB-7105 board led configuration
 *
 * @return none
 */
void stb_7105_config_led(void)
{
	/**
	 * STANDBY_LED1 is blue and connected to 5/2
	 * STANDBY_LED2 is red and connected to 5/3
	 */
	SET_PIO_PIN(PIO_PORT(5), 2, STPIO_OUT);
	SET_PIO_PIN(PIO_PORT(5), 3, STPIO_OUT);

	/* turn on blue LEDs */
	STPIO_SET_PIN(PIO_PORT(5), 2, 1);
	STPIO_SET_PIN(PIO_PORT(5), 3, 0);

	/**
	 * other LEDS
	 * ----------
	 * CON_LED is on 4/2 (output, low level = off)
	 * MOD_LED is on 4/3 (output, low level = off)
	 * IR_LED is on 5/1 (output, low level = off)
	 * WLAN_LED is on 5/4 (output, low level = off)
	 * REC_LED is on 5/5 (output, low level = off)
	 * think about to config unused LED's as input
	 * according STB type.
	 */
	STPIO_SET_PIN(PIO_PORT(4), 2, 0);
	SET_PIO_PIN(PIO_PORT(4), 2, STPIO_OUT);

	STPIO_SET_PIN(PIO_PORT(4), 3, 0);
	SET_PIO_PIN(PIO_PORT(4), 3, STPIO_OUT);

	STPIO_SET_PIN(PIO_PORT(5), 1, 0);
	SET_PIO_PIN(PIO_PORT(5), 1, STPIO_OUT);

	STPIO_SET_PIN(PIO_PORT(5), 4, 0);
	SET_PIO_PIN(PIO_PORT(5), 4, STPIO_OUT);

	STPIO_SET_PIN(PIO_PORT(5), 5, 0);
	SET_PIO_PIN(PIO_PORT(5), 5, STPIO_OUT);
}

/**
 * STB-7105 board pre init functions
 *
 * @return none
 */
void stb_7105_config_pre(void)
{
	unsigned long sysconf;

	/* Setup PIO of ASC device */
#if !defined(__SEC_LEVEL_STRICT__)
	SET_PIO_PIN(PIO_PORT(4), 0, STPIO_ALT_OUT);	/* UART2, RS232 TXD */
	SET_PIO_PIN(PIO_PORT(4), 1, STPIO_IN);		/* UART2, RS232 RXD */
#else
	SET_PIO_PIN(PIO_PORT(4), 0, STPIO_IN);
	SET_PIO_PIN(PIO_PORT(4), 1, STPIO_OUT);
#endif
	/* Select UART2 via PIO4 */
	sysconf = *STX7105_SYSCONF_SYS_CFG07;
	/* CFG07[1] = UART2_RXD_SRC_SELECT = 0 */
	/* CFG07[2] = UART2_CTS_SRC_SELECT = 0 */
	sysconf &= ~(1ul<<2 | 1ul<<1);
	*STX7105_SYSCONF_SYS_CFG07 = sysconf;

	/* Route UART2 via PIO4 for TX, RX, CTS & RTS */
	sysconf = *STX7105_SYSCONF_SYS_CFG34;
	/* PIO4[0] CFG34[8,0]   AltFunction = 3 */
	/* PIO4[1] CFG34[9,1]   AltFunction = 3 */
	/* PIO4[2] CFG34[10,2]  AltFunction = 3 */
	/* PIO4[3] CFG34[11,3]  AltFunction = 3 */
	sysconf &= ~0x0f0ful;	/* 3,3,3,3 */
	sysconf |=  0x0f00ul;	/* 2,2,2,2 */
	*STX7105_SYSCONF_SYS_CFG34 = sysconf;

//	/* Route UART3 via PIO5 for TX, RX, CTS & RTS */
//	sysconf = *STX7105_SYSCONF_SYS_CFG35;
//	/* PIO5[0] CFG35[8,0]   AltFunction = 3 */
//	/* PIO5[1] CFG35[9,1]   AltFunction = 3 */
//	/* PIO5[2] CFG35[10,2]  AltFunction = 3 */
//	/* PIO5[3] CFG35[11,3]  AltFunction = 3 */
//	sysconf &= ~0x0f0ful;	/* 3,3,3,3 */
//	sysconf |=  0x000ful;	/* 1,1,1,1 */
//	*STX7105_SYSCONF_SYS_CFG35 = sysconf;

#if 1
	/* disable USB power */
	SET_PIO_PIN(PIO_PORT(4), 5, STPIO_OUT);
	STPIO_SET_PIN(PIO_PORT(4), 5, 0);

	SET_PIO_PIN(PIO_PORT(4), 7, STPIO_OUT);
	STPIO_SET_PIN(PIO_PORT(4), 7, 0);
#endif

	/* STANDBY/POWER_OFF as input */
	SET_PIO_PIN(PIO_PORT(2), 1, STPIO_IN);

	/**
	 * Hardware reset generation output
	 * -----------
	 * HW_RST is on 5/0 (input)
	 */
	SET_PIO_PIN(PIO_PORT(5), 0, STPIO_IN);

	/**
	 * Standby for 3.3V/5V
	 * -----------
	 * STANDBY is on 3/1 (output, high level normal mode)
	 */
	SET_PIO_PIN(PIO_PORT(3), 1, STPIO_OUT);
	STPIO_SET_PIN(PIO_PORT(3), 1, 1);

	/**
	 * NAND flash WP#
	 * -----------
	 * FLASH_WP is on 1/2 (output)
	 */
	SET_PIO_PIN(PIO_PORT(1), 2, STPIO_OUT);

}

/**
 * STB-7105 board misc init functions
 *
 * @return none
 */
void stb_7105_config_misc(void)
{
	/**
	 * IR receiver
	 * -----------
	 * IR is on 3/0
	 */
	SET_PIO_PIN(PIO_PORT(3), 0, STPIO_IN);
}

/**
 * STB-7105 board frontpanel keypad config
 *
 * @return none
 */
void stb_7105_config_keypad(void)
{
	/**
	 * Keypad used on front panel
	 * --------------------------
	 * KEY_DOWN is on 11/2 (input)
	 * KEY_LEFT is on 11/3 (input)
	 * KEY_RIGHT is on 11/4 (input)
	 * KEY_MENU is on 11/5 (input)
	 * KEY_LEFT is on 11/6 (input)
	 * KEY_LEFT is on 11/7 (input)
	 * KEY_UP is on 5/7 (input)
	 */
	SET_PIO_PIN(PIO_PORT(11), 2, STPIO_IN);
	SET_PIO_PIN(PIO_PORT(11), 3, STPIO_IN);
	SET_PIO_PIN(PIO_PORT(11), 4, STPIO_IN);
	SET_PIO_PIN(PIO_PORT(11), 5, STPIO_IN);
	SET_PIO_PIN(PIO_PORT(11), 6, STPIO_IN);
	SET_PIO_PIN(PIO_PORT(11), 7, STPIO_IN);
	SET_PIO_PIN(PIO_PORT(5), 7, STPIO_IN);
}


#if (DISPLAY_VFD==2)
/**
 * send a command to the HT16515
 *
 * @param cmd [IN] unchar, command value
 * @param clear_display_ram [IN] unchar, a value of 1 will clear the display ram
 * @return none
 */
static void ht16515_send_command(unchar cmd, int clear_display_ram)
{
	unchar bit_loop;
	unchar bit;
	unchar cnt;

	// set STB to LOW
	STPIO_SET_PIN(PIO_PORT(15), 2, HT16515_PIN_LOW);

	// wait clock strobe time
	udelay(HT16515_TIMING_CLKSTB);

	// Send command bit by bit. First bit must be the LSB.
	for(bit_loop=0; bit_loop < 8; bit_loop++ )
	{
		// read bit
		bit = cmd & (1 << bit_loop);

		// force CLK low
		STPIO_SET_PIN(PIO_PORT(15), 0, HT16515_PIN_LOW);

		// Set DIN depending on the bit read out (=1 -> HIGH, =0 -> LOW)
		STPIO_SET_PIN(PIO_PORT(15), 1, bit ? HT16515_PIN_HIGH : HT16515_PIN_LOW);

		// wait minimal clock pulse with
		udelay(HT16515_TIMING_PWCLK);

		// force CLK high
		STPIO_SET_PIN(PIO_PORT(15), 0, HT16515_PIN_HIGH);

		// wait minimal clock pulse with
		udelay(HT16515_TIMING_PWCLK);
	}

	// wait (strobe pulse width)
	udelay(HT16515_TIMING_PWSTB);

	// if no data has to be sent after the already executed command -> exit
	if(clear_display_ram == 0)
	{	// set STB to HIGH
		STPIO_SET_PIN(PIO_PORT(15), 2, HT16515_PIN_HIGH);
		return;
	}

	// Send data: clear all the display ram
	for(cnt = 0; cnt < HT16515_RAM_SIZE; cnt++)
	{
		// clear all the segments of all the digits
		cmd = 0x00;

		// Send command bit by bit. First bit must be the LSB.
		for(bit_loop=0; bit_loop < 8; bit_loop++ )
		{
			// read bit
			bit = cmd & (1 << bit_loop);

			// force CLK low
			STPIO_SET_PIN(PIO_PORT(15), 0, HT16515_PIN_LOW);

			// Set DIN depending on the bit read out (=1 -> HIGH, =0 -> LOW)
			STPIO_SET_PIN(PIO_PORT(15), 1, bit ? HT16515_PIN_HIGH : HT16515_PIN_LOW);

			// wait minimal clock pulse with
			udelay(HT16515_TIMING_PWCLK);

			// force CLK high
			STPIO_SET_PIN(PIO_PORT(15), 0, HT16515_PIN_HIGH);

			// wait minimal clock pulse with
			udelay(HT16515_TIMING_PWCLK);
		}

		// wait (strobe pulse width)
		udelay(HT16515_TIMING_PWSTB);
	}

	// wait clock strobe time
	udelay(HT16515_TIMING_CLKSTB);

	// set STB to HIGH
	STPIO_SET_PIN(PIO_PORT(15), 2, HT16515_PIN_HIGH);

	// wait (strobe pulse width)
	udelay(HT16515_TIMING_PWSTB);

}

/**
 * Initialization of VFD Controller HT16515
 *
 * @return none
 */
static void ht16515_init(void)
{
	ht16515_send_command(HT16515_CMD2_INIT, 0);
	ht16515_send_command(HT16515_CMD3_INIT, 1);
	ht16515_send_command(HT16515_CMD1_INIT, 0);
	ht16515_send_command(HT16515_CMD4_INIT, 0);
}
#endif

/**
 * STB-7105 board frontpanel VFD config
 *
 * @return none
 */
void stb_7105_config_vfd(void)
{
#if (DISPLAY_VFD==1)
	/**
	 * VFD used on front panel
	 * --------------------------
	 * VFD_RESET is on 5/6 (output, level ????)
	 * VFD_CLOCK is on 15/0 (output)
	 * VFD_DATA is on 15/1 (output)
	 * VFD_CS is on 15/2 (output)
	 */
	SET_PIO_PIN(PIO_PORT(5), 6, STPIO_OUT);
	STPIO_SET_PIN(PIO_PORT(5), 6, 0);

	SET_PIO_PIN(PIO_PORT(15), 0, STPIO_OUT);
	SET_PIO_PIN(PIO_PORT(15), 1, STPIO_OUT);
	SET_PIO_PIN(PIO_PORT(15), 2, STPIO_OUT);
#endif
#if (DISPLAY_VFD==2)
	/**
	 * Configuration of VFD Controller HT16515 (controls VFD on front panel)
	 * --------------------------------------------------------------------
	 * VFD_CLOCK is on 15/0 (output) Clock Input Pin of HT16515
	 * VFD_DI is on 15/1 (output) Data Input Pin of HT16515
	 * VFD_CS is on 15/2 (output) Serial Interface Strobe (STB) Pin of HT16515
	 * VFD_DO is on 15/3 (input) Data Output Pin of HT16515
	 */
	SET_PIO_PIN(PIO_PORT(15), 0, STPIO_OUT);
	SET_PIO_PIN(PIO_PORT(15), 1, STPIO_OUT);
	SET_PIO_PIN(PIO_PORT(15), 2, STPIO_OUT);
	SET_PIO_PIN(PIO_PORT(15), 3, STPIO_IN);

	/* initialize VFD Controller HT16515 */
	ht16515_init();
#endif
}

/**
 * STB-7105 board DVB-X config
 *
 * @return none
 */
static void stb_7105_config_dvb(void)
{
#if defined(STB_8100)
	/**
	 * DVB-X tuner reset (low active)
	 * ------------------------------
	 * DVB Tuner A Reset is on 7/0 (FEA_RSTn)
	 * DVB Tuner B Reset is on 7/1 (FEB_RSTn)
	 * The RST pin on TU1211 is internal pulled up to +5V this means
	 * that we shall use tristate for deassert reset
	 */
	SET_PIO_PIN(PIO_PORT(7), 0, STPIO_OUT);
	STPIO_SET_PIN(PIO_PORT(7), 0, 0);

	SET_PIO_PIN(PIO_PORT(7), 1, STPIO_OUT);
	STPIO_SET_PIN(PIO_PORT(7), 1, 0);

	udelay(DVB_RESET_TIME);

	SET_PIO_PIN(PIO_PORT(7), 0, STPIO_IN);
	SET_PIO_PIN(PIO_PORT(7), 1, STPIO_IN);
#else
	/**
	 * DVB-X tuner reset (low active)
	 * ------------------------------
	 * DVB Tuner A Reset is on 3/2 (FEA_RSTn)
	 * DVB Tuner B Reset is on 3/3 (FEB_RSTn)
	 * The RST pin on TU1211 is internal pulled up to +5V this means
	 * that we shall use tristate for deassert reset
	 */
	SET_PIO_PIN(PIO_PORT(3), 2, STPIO_OUT);
	STPIO_SET_PIN(PIO_PORT(3), 2, 0);

	SET_PIO_PIN(PIO_PORT(3), 3, STPIO_OUT);
	STPIO_SET_PIN(PIO_PORT(3), 3, 0);

	udelay(DVB_RESET_TIME);

	SET_PIO_PIN(PIO_PORT(3), 2, STPIO_IN);
	SET_PIO_PIN(PIO_PORT(3), 3, STPIO_IN);
#endif
}

/**
 * STB-7105 board CI config
 *
 * @return none
 */
static void stb_7105_config_ci(void)
{
	/**
	 * CI StarCI2Win Reset
	 * ------------------------------
	 * Reset is on 1/7 and it is active high.
	 * we leave StarCI2Win in reset.
	 */
}

/**
 * STB-7105 board misc init functions
 *
 * @return none
 */
void stb_7105_config_power(void)
{
	/**
	 * Keeping main power switch on
	 * --------
	 * POWER_ON is on 2/0 (output, high level = on)
	 */
	STPIO_SET_PIN(PIO_PORT(2), 0, 1);
	SET_PIO_PIN(PIO_PORT(2), 0, STPIO_OUT);

	/**
	 * HDD power switch on (and +12V for VFD)
	 * --------
	 * HDD_POWER is on 1/6 (output, high level = on)
	 */
	STPIO_SET_PIN(PIO_PORT(1), 6, 1);
	SET_PIO_PIN(PIO_PORT(1), 6, STPIO_OUT);
}

/**
 * STB-7105 board init function
 *
 * @return unused
 */
int stb_7105_board_init(void)
{
	uchar hbi;

	stb_7105_config_power();

	stb_7105_config_pre();

	hbi = stb_7105_get_hbi();

	stb_7105_config_led();

	stb_7105_config_i2c_busses();

	/* Configure & Reset the Ethernet PHY */
	stb_7105_config_ethernet();

	stb_7105_stv6419_init();

	/**
	 * TODO
	 * - Standby
	 */
	stb_7105_config_keypad();

	stb_7105_config_vfd();

	stb_7105_config_dvb();

	stb_7105_config_ci();

	stb_7105_config_misc();

#if defined(CONFIG_STM_SATA)
	stx7105_configure_sata ();
#endif	/* CONFIG_STM_SATA */

	return 0;
}
