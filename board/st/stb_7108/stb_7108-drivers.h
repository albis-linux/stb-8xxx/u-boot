/*
 * (C) Copyright 2011-2013 ALBIS Technologies.
 *
 * Michael Schenk <michael.schenk@albistechnologies.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef _STB_7108_DRIVERS_H_
#define _STB_7108_DRIVERS_H_

#include <config.h>

/*----------------------------------------------------------------------
 * public functions declaration
 */
extern int stb_7108_force_recovery_boot_check(void);
extern int stb_7108_board_init(void);
extern uchar stb_7108_get_hbi(void);

#define mdelay(n) 							({unsigned long msec=(n); while (msec--) udelay(1000);})

/**
 * Recovery boot check
 */	
#define RECOVER_BOOT_CHECK_TIME				5000	/* check for 5000 ms */
#define RECOVER_BOOT_CHECK_INTERVAL			200		/* check every 200 ms */

/**
 * memory test check
 */	
#define MEMORY_TEST_CHECK_TIME				5000	/* check for 5000 ms */
#define MEMORY_TEST_CHECK_INTERVAL			200		/* check every 200 ms */



/*----------------------------------------------------------------------
 * Audio Video Switch Visual Output STV6430 default values
 * according datasheet
 */

#define STV6430_I2C_ADDRESS 				0x4A

#define STV6430_REG_WR0						0x00
#define STV6430_REG_WR1						0x01
#define STV6430_REG_WR2						0x02
#define STV6430_REG_WR3						0x03
#define STV6430_REG_WR4						0x04

/*----------------------------------
 * WR0 - audio and video control
 * +6dB for weak audio inputs			(D5 = 1, D6 = 0, D7 = 0)
 */ 
#define STV6430_REG_WR0_VALUE			0x20

/*----------------------------------
 * WR1 - miscellaneous control
 * SD video 6db gain is selected 		(D0 = 0)
 * CVBS input to CVBS output 			(D2 = 0)
 * R/C input to R/C output 				(D3 = 0)
 * Not used 							(D1,D4,D5,D6,D7 = 0)
 */ 
#define STV6430_REG_WR1_VALUE			0x00


/* M.Schenk 2012.02.06 remark, change audio to mute in future...*/

/*----------------------------------
 * WR2 - mute control
 * B mute control active 				(D3 = 0)
 * G mute control active 				(D4 = 0)
 * R/C mute control active 				(D5 = 0)
 * CVBS mute control active 			(D6 = 0)
 * Audio mute control active 			(D7 = 0)
 * Not used 							(D0,D1,D2 = 0)
 */
#ifndef STV6430_AUDIO_MUTED
#define STV6430_REG_WR2_VALUE			0x00
#else
#define STV6430_REG_WR2_VALUE			0x80
#endif

/*----------------------------------
 * WR3 - zero-cross detection, fast and slow blanking control
 * SB TV output is 16:9					(D0 = 1, D1 = 0)
 * FB is forced to high level 			(D2 = 1)
 * Slow blanking is off					(D3 = 1)
 * Fast blanking is on					(D4 = 1)
 * Zero-cross detection is active		(D7 = 0)
 * Not used 							(D5,D6 = 0)
 */ 
#define STV6430_REG_WR3_VALUE			0x0d

/*----------------------------------
 * WR4 - standby mode
 * B video channel is ON				(D3 = 0)
 * G video channel is ON				(D4 = 0)
 * R/C video channel is ON				(D5 = 0)
 * CVBS video channel is ON				(D6 = 0)
 * Audio channel is ON					(D7 = 0)
 * Not used 							(D0,D1,D3 = 0)
 */ 
#define STV6430_REG_WR4_VALUE			0x00

/**
 * DVB-X tuner defines
 */	
#define DVB_RESET_TIME					100

#endif
