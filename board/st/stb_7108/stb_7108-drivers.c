/*
 * (C) Copyright 2011-2013 ALBIS Technologies.
 *
 * Michael Schenk <michael.schenk@albistechnologies.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <command.h>
#include <stm/soc.h>
#include <stm/stx7108reg.h>
#include <asm/io.h>
#include <stm/pio.h>
#include <i2c.h>
#include "stb_7108-i2c.h"
#include "stb_7108-drivers.h"



#define PIOALT(port, pin, alt, dir)					\
do													\
{													\
	stx7108_pioalt_select((port), (pin), (alt));	\
	stx7108_pioalt_pad((port), (pin), (dir));		\
} while(0)


/**
 * check recovery force booting switch (standby switch)
 *
 * @return 0 not pressed, 1 presses
 */
int stb_7108_force_recovery_boot_check(void)
{
	int cnt;

	/* standby switch is on 26/7 */

	if (STPIO_GET_PIN(STM_PIO_BASE(26), 7) != 0)
		return 0;

	cnt = RECOVER_BOOT_CHECK_TIME / RECOVER_BOOT_CHECK_INTERVAL;

	while (cnt-- != 0) {
		mdelay(RECOVER_BOOT_CHECK_INTERVAL);
		if (STPIO_GET_PIN(STM_PIO_BASE(26), 7) != 0)
			return 0;
	}

	return 1;
}

/**
 * Init function for Audio/Video Switch Chip STV6430
 *
 * @return none
 */
static void stb_7108_stv6430_init(void)
{
	/*
	 * init i2c Bus
	 */
	stb_7108_i2c_init(EXT_CONFIG_SYS_I2C_SPEED, EXT_CFG_I2C_SLAVE);

	/*
	 * set init valus of the STV6430 Registers
	 */
	stb_7108_i2c_reg_write(STV6430_I2C_ADDRESS, STV6430_REG_WR0,
						   STV6430_REG_WR0_VALUE);
	stb_7108_i2c_reg_write(STV6430_I2C_ADDRESS, STV6430_REG_WR1,
						   STV6430_REG_WR1_VALUE);
	stb_7108_i2c_reg_write(STV6430_I2C_ADDRESS, STV6430_REG_WR2,
						   STV6430_REG_WR2_VALUE);
	stb_7108_i2c_reg_write(STV6430_I2C_ADDRESS, STV6430_REG_WR3,
						   STV6430_REG_WR3_VALUE);
	stb_7108_i2c_reg_write(STV6430_I2C_ADDRESS, STV6430_REG_WR4,
						   STV6430_REG_WR4_VALUE);
}

#ifdef CONFIG_DRIVER_NET_STM_GMAC
extern void stmac_phy_reset(void)
{
	/*
	 * Reset the Ethernet PHY.
	 * Note both PHYs share the *same* reset line.
	 *
	 *	PIO15[4] = POWER_ON_ETH (a.k.a. ETH_RESET)
	 */
	STPIO_SET_PIN(STM_PIO_BASE(15), 4, 0);
	udelay(10000);				/* 10 ms */
	STPIO_SET_PIN(STM_PIO_BASE(15), 4, 1);
}
#endif	/* CONFIG_DRIVER_NET_STM_GMAC */

/**
 * STB-7108 ethernet configuration
 *
 * @return none
 */
static void stb_7108_config_ethernet(void)
{

#ifdef CONFIG_DRIVER_NET_STM_GMAC
#if CONFIG_SYS_STM_STMAC_BASE == CONFIG_SYS_STM_STMAC0_BASE	/* MII0, on MII JP2 */
	stx7108_configure_ethernet(0, &(struct stx7108_ethernet_config) {
			.mode = stx7108_ethernet_mode_mii,
			.ext_clk = 1,
			.phy_bus = 0, });
#elif CONFIG_SYS_STM_STMAC_BASE == CONFIG_SYS_STM_STMAC1_BASE	/* MII1, IC+ IP1001 (UP1) */
	stx7108_configure_ethernet(1, &(struct stx7108_ethernet_config) {
			.mode = stx7108_ethernet_mode_mii,	/* for MII */
//			.mode = stx7108_ethernet_mode_rmii,	/* for RMII */
			.ext_clk = 0,
			.phy_bus = 1, });
#else
#error Unknown base address for the STM GMAC
#endif
	/* Hard Reset the PHY -- do after we have configured the MAC */
	stmac_phy_reset();
#endif	/* CONFIG_DRIVER_NET_STM_GMAC */


}

/**
 * return value of hardware boot identifier
 *
 * @return hbi value
 */
uchar stb_7108_get_hbi(void)
{
	uchar hbi_value = 0;

	/**
	 * Hardware boot identifier (HBI)
	 * ------------------------------
	 * HW3 is on 11/6 (input)
	 * HW2 is on 11/5 (input)
	 * HW1 is on 11/3 (input)
	 * HW0 is on 11/4 (input)
	 */
	SET_PIO_PIN(STM_PIO_BASE(11), 3, STPIO_IN);
	SET_PIO_PIN(STM_PIO_BASE(11), 4, STPIO_IN);
	SET_PIO_PIN(STM_PIO_BASE(11), 5, STPIO_IN);
	SET_PIO_PIN(STM_PIO_BASE(11), 6, STPIO_IN);

	hbi_value = ((STPIO_GET_PIN(STM_PIO_BASE(11), 6) << 3) | (STPIO_GET_PIN(STM_PIO_BASE(11), 5) << 2) |
				 (STPIO_GET_PIN(STM_PIO_BASE(11), 3) << 1) | STPIO_GET_PIN(STM_PIO_BASE(11), 4));

	return hbi_value;
}

#if 0
/**
 * STB-7108 smart card configuration
 *
 * @return none
 */
static void stb_7108_config_smart_card(void)
{
	/**
	 * Smart card configuration
	 * ------------------------
	 * SC0DATAOUT_1 is on 4/0 (bidirectional)
	 * SC0DATAIN_1 is on 4/1 (input)
	 * SC0EXTCLK_1 is on 4/2 (input)
	 * SC0CLK_1 is on 4/3 (output)
	 * SC0RESET_1 is on 4/4 (output)
	 * SC0CMDVCC_1 is on 4/5 (output)
	 * 5V3V_1 is on 4/6 (output)
	 * SC0DET_1 is on 4/7 (input)
	 */
	SET_PIO_PIN(STM_PIO_BASE(4), 0, STPIO_BIDIR);
	SET_PIO_PIN(STM_PIO_BASE(4), 1, STPIO_IN);
	SET_PIO_PIN(STM_PIO_BASE(4), 2, STPIO_IN);
	SET_PIO_PIN(STM_PIO_BASE(4), 3, STPIO_OUT);
	SET_PIO_PIN(STM_PIO_BASE(4), 4, STPIO_OUT);
	SET_PIO_PIN(STM_PIO_BASE(4), 5, STPIO_OUT);
	SET_PIO_PIN(STM_PIO_BASE(4), 6, STPIO_OUT);
	SET_PIO_PIN(STM_PIO_BASE(4), 7, STPIO_IN);
}
#endif
/**
 * STB-7108 I2C configuration
 *
 * @return none
 */
static void stb_7108_config_i2c_busses(void)
{
	/**
	 * I2C bus 6 for HDMI
	 * ------------------------
	 * I2C_SCL6 is on 15/2 (output, high level)
	 * I2C_SDA6 is on 15/3 (input, high level)
	 */
	STPIO_SET_PIN(STM_PIO_BASE(15), 2, 1);
	STPIO_SET_PIN(STM_PIO_BASE(15), 3, 1);
	SET_PIO_PIN(STM_PIO_BASE(15), 2, STPIO_OUT);
	SET_PIO_PIN(STM_PIO_BASE(15), 3, STPIO_IN);

	/**
	 * I2C bus 1 for TUNER1/TUNER2
	 * ---------------------------
	 * I2C_SCL1 is on 9/6 (output, high level)
	 * I2C_SDA1 is on 9/7 (input, high level)
	 */
	STPIO_SET_PIN(STM_PIO_BASE(9), 6, 1);
	STPIO_SET_PIN(STM_PIO_BASE(9), 7, 1);
	SET_PIO_PIN(STM_PIO_BASE(9), 6, STPIO_OUT);
	SET_PIO_PIN(STM_PIO_BASE(9), 7, STPIO_IN);

	/**
	 * I2C bus 5 for EEPROM
	 * --------------------
	 * I2C_SCL5 is on 5/6 (output, high level)
	 * I2C_SDA5 is on 5/7 (input, high level)
	 */
//	STPIO_SET_PIN(STM_PIO_BASE(5), 6, 1);
//	STPIO_SET_PIN(STM_PIO_BASE(5), 7, 1);
//	SET_PIO_PIN(STM_PIO_BASE(5), 6, STPIO_BIDIR);
//	SET_PIO_PIN(STM_PIO_BASE(5), 7, STPIO_BIDIR);

	/**
	 * I2C bus 2 for SCART SWITCH
	 * --------------------------
	 * I2C_SCL2 is on 14/4 (output, high level)
	 * I2C_SDA2 is on 14/5 (input, high level)
	 */
	STPIO_SET_PIN(STM_PIO_BASE(14), 4, 1);
	STPIO_SET_PIN(STM_PIO_BASE(14), 5, 1);
	SET_PIO_PIN(STM_PIO_BASE(14), 4, STPIO_BIDIR);
	SET_PIO_PIN(STM_PIO_BASE(14), 5, STPIO_BIDIR);

#if defined(CONFIG_CMD_I2C)
	stx7108_configure_i2c();
#endif	/* CONFIG_CMD_I2C */
}

/**
 * STB-7108 board led configuration
 *
 * @return none
 */
void stb_7108_config_led(void)
{
	/**
	 * Power LEDs
	 * LED_STB_R Red connected to 3/3
	 * LED_STB_B Blue connected to 3/4
	 */

	/* turn off red LED */
	STPIO_SET_PIN(STM_PIO_BASE(3), 3, 0);

	/* turn on blue LED */
	STPIO_SET_PIN(STM_PIO_BASE(3), 4, 1);

	SET_PIO_PIN(STM_PIO_BASE(3), 3, STPIO_OUT);
	SET_PIO_PIN(STM_PIO_BASE(3), 4, STPIO_OUT);

	/**
	 * IR LEDs
	 * LED_IR_R LED red connected to 3/0
	 * LED_IR_W LED white connected to 3/1
	 */
	STPIO_SET_PIN(STM_PIO_BASE(3), 0, 0);
	STPIO_SET_PIN(STM_PIO_BASE(3), 1, 0);

	SET_PIO_PIN(STM_PIO_BASE(3), 0, STPIO_OUT);
	SET_PIO_PIN(STM_PIO_BASE(3), 1, STPIO_OUT);


#if 0
	stx7108_pioalt_select(26, 4, 0);			/* PIO */
	stx7108_pioalt_pad(26, 4, stm_pad_direction_output);

	STPIO_SET_PIN(STM_PIO_BASE(26), 4, 1);

	SET_PIO_PIN(STM_PIO_BASE(26), 4, STPIO_OUT);
#endif
}

/**
 * STB-7108 board pre init functions
 *
 * @return none
 */
void stb_7108_config_pre(void)
{
	/* Setup PIOs for ASC device */

#if CONFIG_SYS_STM_ASC_BASE == STM_ASC1_REGS_BASE

	/* Route UART1 via PIO5 for TX, RX, CTS & RTS (Alternative #1) */
	PIOALT(5, 1, 1, stm_pad_direction_output);	/* UART1-TX */
	PIOALT(5, 2, 1, stm_pad_direction_input);	/* UART1-RX */
//	PIOALT(5, 4, 1, stm_pad_direction_output);	/* UART1-RTS */
//	PIOALT(5, 3, 1, stm_pad_direction_input);	/* UART1-CTS */

#elif CONFIG_SYS_STM_ASC_BASE == STM_ASC3_REGS_BASE

	/* Route UART3 via PIO24/25 for TX, RX (Alternative #1) */
	PIOALT(24, 4, 1, stm_pad_direction_output);	/* UART3-TX */
	PIOALT(24, 5, 1, stm_pad_direction_input);	/* UART3-RX */
//	PIOALT(24, 7, 1, stm_pad_direction_output);	/* UART3-RTS */
//	PIOALT(25, 0, 1, stm_pad_direction_input);	/* UART3-CTS */

#else
#error Unknown ASC port selected!
#endif	/* CONFIG_SYS_STM_ASC_BASE == STM_ASCx_REGS_BASE */

#ifdef CONFIG_DRIVER_NET_STM_GMAC
	/*
	 * Configure the Ethernet PHY Reset signal
	 *	PIO15[4] == POWER_ON_ETH (a.k.a. ETH_RESET)
	 */
	SET_PIO_PIN(STM_PIO_BASE(15), 4, STPIO_OUT);
#endif	/* CONFIG_DRIVER_NET_STM_GMAC */



#if 1
	/* disable USB power */

	/* USB0 */
	STPIO_SET_PIN(STM_PIO_BASE(23), 7, 0);
	SET_PIO_PIN(STM_PIO_BASE(23), 7, STPIO_OUT);

	/* USB1 */
	STPIO_SET_PIN(STM_PIO_BASE(24), 1, 0);
	SET_PIO_PIN(STM_PIO_BASE(24), 1, STPIO_OUT);

	/* USB2 */
	STPIO_SET_PIN(STM_PIO_BASE(24), 3, 0);
	SET_PIO_PIN(STM_PIO_BASE(24), 3, STPIO_OUT);
#endif



	/* STANDBY/POWER_OFF as input 26/7 */
	SET_PIO_PIN(STM_PIO_BASE(26), 7, STPIO_IN);


	/**
	 * Hardware reset generation output
	 * -----------
	 * SW_RSTn is on 3/5 (input)
	 */
	SET_PIO_PIN(STM_PIO_BASE(3), 5, STPIO_IN);

	/**
	 * NAND flash WP#
	 * -----------
	 * FLASH_WP is on 5/5 (output)
	 */
	SET_PIO_PIN(STM_PIO_BASE(5), 5, STPIO_OUT);
}

/**
 * STB-7108 board misc init functions
 *
 * @return none
 */
void stb_7108_config_misc(void)
{
	/**
	 * IR receiver
	 * -----------
	 * IR is on 2/7
	 */
	SET_PIO_PIN(STM_PIO_BASE(2), 7, STPIO_IN);
}

/**
 * STB-7108 board frontpanel keypad config
 *
 * @return none
 */
void stb_7108_config_keypad(void)
{
	/* STB-7108 doesn't have a keypad */
}

/**
 * STB-7108 board frontpanel VFD config
 *
 * @return none
 */
void stb_7108_config_vfd(void)
{
#if (DISPLAY_VFD==1)
	/**
	 * VFD used on front panel
	 * --------------------------
	 * VFD_RESET is on 3/2 (output, level ????)
	 * VFD_CLK is on 2/4 (output)
	 * VFD_DATA is on 2/5 (output)
	 * VFD_CS is on 2/6 (output)
	 */
	STPIO_SET_PIN(STM_PIO_BASE(3), 2, 0);
	SET_PIO_PIN(STM_PIO_BASE(3), 2, STPIO_OUT);

	SET_PIO_PIN(PIO_PORT(2), 4, STPIO_OUT);
	SET_PIO_PIN(PIO_PORT(2), 5, STPIO_OUT);
	SET_PIO_PIN(PIO_PORT(2), 6, STPIO_OUT);
#endif
}

/**
 * STB-7108 board DVB-X config
 *
 * @return none
 */
static void stb_7108_config_dvb(void)
{
#if defined(STB_9300)
	/**
	 * DVB-X tuner reset (low active)
	 * ------------------------------
	 * DVB Tuner 0 Reset is on 9/4 (FE0_RSTn)
	 * DVB Tuner 1 Reset is on 9/5 (FE1_RSTn)
	 * The RST pin are pulled up to +3.3V this means
	 * that we shall use tristate for deassert reset
	 */
	STPIO_SET_PIN(STM_PIO_BASE(9), 4, 0);
	STPIO_SET_PIN(STM_PIO_BASE(9), 5, 0);

	SET_PIO_PIN(STM_PIO_BASE(9), 4, STPIO_OUT);
	SET_PIO_PIN(STM_PIO_BASE(9), 5, STPIO_OUT);

	udelay(DVB_RESET_TIME);

	SET_PIO_PIN(STM_PIO_BASE(9), 4, STPIO_IN);
	SET_PIO_PIN(STM_PIO_BASE(9), 5, STPIO_IN);

#endif
}

/**
 * STB-7108 board misc init functions
 *
 * @return none
 */
void stb_7108_config_power(void)
{
	/**
	 * HDD power switch on (and +12V for VFD)
	 * --------------------------------------
	 * HPS is on 5/0 (output, high level = on)
	 */
	STPIO_SET_PIN(STM_PIO_BASE(5), 0, 1);
	SET_PIO_PIN(STM_PIO_BASE(5), 0, STPIO_OUT);
}

/**
 * STB-7108 board init function
 *
 * @return unused
 */
int stb_7108_board_init(void)
{
	stb_7108_config_power();

	stb_7108_config_pre();

	stb_7108_get_hbi();

//	stb_7108_config_led();

	stb_7108_config_i2c_busses();

	/* Configure & Reset the Ethernet PHY */
	stb_7108_config_ethernet();

	stb_7108_stv6430_init();

	stb_7108_config_keypad();

	stb_7108_config_vfd();

	stb_7108_config_dvb();

	stb_7108_config_misc();

#if defined(CONFIG_STM_SATA)
	stx7105_configure_sata ();
#endif	/* CONFIG_STM_SATA */

	return 0;
}
