/*
 * (C) Copyright 2011-2013 ALBIS Technologies.
 *
 * Michael Schenk <michael.schenk@albistechnologies.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef _STB_7108_I2C_H_
#define _STB_7108_I2C_H_

#include <config.h>

void stb_7108_i2c_init(int speed, int slaveaddr);
/*
 * Read/Write interface:
 *   chip:    I2C chip address, range 0..127
 *   addr:    Memory (register) address within the chip
 *   alen:    Number of bytes to use for addr (typically 1, 2 for larger
 *              memories, 0 for register type devices with only one
 *              register)
 *   buffer:  Where to read/write the data
 *   len:     How many bytes to read/write
 *
 *   Returns: 0 on success, not 0 on failure
 */
int stb_7108_i2c_read(uchar chip, uint addr, int alen, uchar *buffer, int len);
int stb_7108_i2c_write(uchar chip, uint addr, int alen, uchar *buffer, int len);

/*
 * Utility routines to read/write registers.
 */
uchar stb_7108_i2c_reg_read (uchar chip, uchar reg);
void  stb_7108_i2c_reg_write(uchar chip, uchar reg, uchar val);


#endif
