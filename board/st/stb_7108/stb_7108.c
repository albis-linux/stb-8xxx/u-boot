/*
 * (C) Copyright 2008-2012 STMicroelectronics.
 *
 * Sean McGoogan <Sean.McGoogan@st.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */
#include <common.h>
#include <command.h>
#include <environment.h>
#include <config.h>
#include <nand.h>
#include <jffs2/jffs2.h>
#include <stm/soc.h>
#include <stm/stx7108reg.h>
#include <asm/io.h>
#include <stm/pio.h>
#include "stb_7108-drivers.h"
#include <i2c.h>
#include <gpg-v3-verify.h>

#define IMAGE_HEADER_SIZE 	sizeof(image_header_t)

extern nand_info_t nand_info[];

extern int memory_test(unsigned long *block_start, unsigned long block_length);
extern int memory_test_albis(unsigned long *block_start, unsigned long block_length);
extern int memory_test_stats_albis(void);

/* parition handling routines */
int mtdparts_init(void);
int find_dev_and_part(const char *id, struct mtd_device **dev,
					  u8 *part_num, struct part_info **part);

/**
 * Enable NAND flash write
 *
 * @return none
 */
void flashWriteEnable(void)
{
	/* Disable WP for writing to NAND flash */
	STPIO_SET_PIN(STM_PIO_BASE(5), 5, 1);
}

/**
 * Disable NAND flash write
 *
 * @return none
 */
void flashWriteDisable(void)
{
	/* keep NAND writeable */
}

/**
 * STB-7108 board init
 *
 * @return unused
 */
extern int board_init(void)
{
	stb_7108_board_init();

	return 0;
}

/**
 * Checking board
 *
 * @return unused
 */
int checkboard (void)
{
	printf ("\n\nBoard: STB7108"
		"  [512/256]"
#ifdef CONFIG_ST40_SE_MODE
		"  [32-bit mode]"
#else
		"  [29-bit mode]"
#endif
		"\n");

	return 0;
}

/**
 * Checking a image (kernel) at a given address
 *
 * @param addr address in RAM to check
 * @param initramfs_header_buffer Pointer to initramfs header data
 * @return 0 valid, 1 invalid
 */
static int image_check(ulong addr, uint8_t* initramfs_header_buffer)
{
#if defined(__STB_SECURE__)
	/* DSA check is enough */
	int rc;
	InitRamFsHeader_t* initramfs_header = (InitRamFsHeader_t*)initramfs_header_buffer;

	printf("## Checking Image at %08lx ... \n", addr);

	/* Provide the header information and the start-address of the image for Signature Verification */
	rc = verifyGpgV3Signature((char*)addr,
							  ntohl(initramfs_header->imageSize),
							  (char*)initramfs_header->digitalSignature,
							  ntohl(initramfs_header->signatureSize));
	if (rc) {
		printf("signature is NOT valid: %s\n",
				rc == SFSV_BAD_SIG ? "bad signature"
						: rc == SFSV_NO_KEY  ? "public key not available"
								: rc == SFSV_NO_SIG  ? "no signature found"
										: rc == SFSV_BAD_ELF ? "invalid elf object"
												: rc == SFSV_BAD_PGP ? "invalid signature packet"
														: "unknown reason" );
		puts ("\nSignature Check Failed!!!\n");
		return 1;
	}

	puts("Ok\n");

	return 0;
#else
	image_header_t header;
	ulong data;
	ulong len;
	ulong checksum;
	image_header_t *hdr = &header;

	printf("## Checking Image at %08lx ... \n", addr);

	/* copy header so we can blank CRC field for re-calculation */
	memmove(&header, (char *)(addr), sizeof(image_header_t));

	if (ntohl(hdr->ih_magic) != IH_MAGIC) {
		puts ("   Bad Magic Number\n");
		return 1;
	} else {
		puts("## Verifying Magic Number... Ok\n");
	}

	data = (ulong)&header;
	len  = sizeof(image_header_t);

	checksum = ntohl(hdr->ih_hcrc);
	hdr->ih_hcrc = 0;

	puts("## Verifying Header Checksum ... ");

	if (crc32(0, (unsigned char *)data, len) != checksum) {
		puts ("   Bad Header Checksum\n");
		return 1;
	} else {
		puts("Ok\n");
	}

	/* for multi-file images we need the data part, too */
	image_print_contents ((image_header_t *)(addr));

	data = (addr) + sizeof(image_header_t);
	len  = ntohl(hdr->ih_size);

	puts("## Verifying Data Checksum ... ");
	if (crc32(0, (unsigned char *)data, len) != ntohl(hdr->ih_dcrc)) {
		puts ("   Bad Data CRC\n");
		return 1;
	} else {
		puts("Ok\n");
	}

	return 0;

#endif
}

/**
 * validate the u-boot environement area
 *
 * @return 0 valid, 1 invalid
 */
static int env_check(void)
{
	puts("ENV:   ");

	/**
	 * Manual override some configuration options, which makes
	 * life easier when chaning some stuff.
	 */
	setenv("board",XSTR(BOARD) "_" XSTR(INPUT_CLOCK_RATE) "\0");
	setenv("monitor_len", XSTR(CONFIG_SYS_MONITOR_LEN) "\0");
	setenv("load_addr", XSTR(CFG_LOAD_ADDR) "\0");

	/* think about to have only a single bootm for main and recovery */

#if !defined(__SEC_LEVEL_STRICT__)
	setenv("execnfskernel", "dhcp " XSTR(KERNEL_DHCP_ADDRESS) "\0");
#endif
	setenv("execmainkernel", "bootm " XSTR(KERNEL_MAIN_ADDRESS) "\0");
	setenv("execrecoverykernel", "bootm " XSTR(KERNEL_RECOVERY_ADDRESS) "\0");

#if defined(__STB_SECURE__)
	/* STB SECURE uses INITRAMFS for main, placed in NAND rootdisk main */
	setenv("execmainbootargs", "setenv bootargs console=ttyAS1,115200 nwhwconf=device:eth0,hwaddr:${ethaddr} root=/dev/initramfs bpa2parts=hole_lx_deltamu:4M:0x80000000: bigphysarea=" XSTR(KERNEL_BIG_PHYS_AREA) "\0");
#else
	setenv("execmainbootargs", "setenv bootargs console=ttyAS1,115200 nwhwconf=device:eth0,hwaddr:${ethaddr} " XSTR(MAIN_ROOT_UBI_MTD)" root=" XSTR(MAIN_ROOT_DEVICE) " rootfstype=" XSTR(MAIN_ROOT_DEVICE_FS) " bpa2parts=hole_lx_deltamu:4M:0x80000000: bigphysarea="  XSTR(KERNEL_BIG_PHYS_AREA) "\0");
#endif
	setenv("execrecoverybootargs", "setenv bootargs console=ttyAS1,115200 nwhwconf=device:eth0,hwaddr:${ethaddr} root=/dev/initramfs bpa2parts=hole_lx_deltamu:4M:0x80000000: bigphysarea=" XSTR(KERNEL_BIG_PHYS_AREA) "\0");


#if !defined(__SEC_LEVEL_STRICT__)
	setenv("execnfsbootargs", "setenv bootargs console=ttyAS1,115200 nwhwconf=device:eth0,hwaddr:${ethaddr} root=/dev/nfs nfsroot=${serverip}:${rootpath},rsize=4096,wsize=8192,tcp ip=${ipaddr}:${serverip}:${gatewayip}:${netmask}:${hostname}::off bpa2parts=hole_lx_deltamu:4M:0x80000000: bigphysarea=" XSTR(KERNEL_BIG_PHYS_AREA) "\0");
#endif

	/**
	 * configuration definition
	 */
	setenv("cfg_offset", XSTR(CONFIG_ENV_OFFSET) "\0");
	setenv("cfg_size", XSTR(CONFIG_ENV_SIZE) "\0");

	/**
	 * due to place kernel in NAND we need some special
	 * cmd to manual load main or recover. The kernel
	 * must first loaded from NAND to memory.
	 */
#if defined(__STB_SECURE__)
	setenv("execforcemainboot", "nboot.jffs2 " XSTR(KERNEL_MAIN_ADDRESS) " " XSTR(KERNEL_MAIN_MTD_PART) " " XSTR(IFS_HEADER_SIZE)"; run execmainbootargs; run execmainkernel\0");
	setenv("execforcerecoveryboot", "nboot.jffs2 " XSTR(KERNEL_RECOVERY_ADDRESS) " " XSTR(KERNEL_RECOVERY_MTD_PART) " " XSTR(IFS_HEADER_SIZE)"; run execrecoverybootargs; run execrecoverykernel\0");
#else
	setenv("execforcemainboot", "nboot.jffs2 " XSTR(KERNEL_MAIN_ADDRESS) " " XSTR(KERNEL_MAIN_MTD_PART)"; run execmainbootargs; run execmainkernel\0");
	setenv("execforcerecoveryboot", "nboot.jffs2 " XSTR(KERNEL_RECOVERY_ADDRESS) " " XSTR(KERNEL_RECOVERY_MTD_PART)"; run execrecoverybootargs; run execrecoverykernel\0");
#endif

	/**
	 * for using MTD parts inside u-boot we first need to load
	 * the defaults.
	 */
	setenv("mtdids", MTDIDS_DEFAULT);
	setenv("mtdparts", MTDPARTS_DEFAULT);
	setenv("partition", NULL);

	/* Do not start kernel automatically -> factoryloader secure */
#if defined(__STB_SECURE__) && defined(__BUILD_TYPE_FACTORY_LOADER__)
	setenv("bootcmd", "");
	setenv("bootdelay", "30");
#else
	setenv("bootdelay", XSTR(CONFIG_BOOTDELAY) "\0");
#endif

	mtdparts_init();

	/**
	 * verify u-boot configuration area
	 */
	{
		ulong crc, len, new;
		unsigned off;
		uchar buf[64];

		/* this will call i2c_init (CFG_I2C_SPEED, CFG_I2C_SLAVE);*/
		eeprom_init ();	/* prepare for EEPROM read/write */

		/* read old CRC */
		eeprom_read (CONFIG_SYS_DEF_EEPROM_ADDR,
					 CONFIG_ENV_OFFSET+offsetof(env_t,crc),
					 (uchar *)&crc, sizeof(ulong));

		new = 0;
		len = ENV_SIZE;
		off = offsetof(env_t,data);
		while (len > 0) {
			int n = (len > sizeof(buf)) ? sizeof(buf) : len;

			eeprom_read (CONFIG_SYS_DEF_EEPROM_ADDR, CONFIG_ENV_OFFSET+off, buf, n);
			new = crc32 (new, buf, n);
			len -= n;
			off += n;
		}

		if (crc == new) {
			puts("is valid\n");
		} else {
			puts("is bad\n");
			return (1);
		}
	}

	return (0);
}

/**
 * Load a kernel from a given partition to a given address.
 *
 * @param partition string describing device and partition or partition name
 * @param addr address in RAM to which kernel will be loaded
 * @param initramfs_header_buffer Pointer to initramfs header data
 * @return 0 on success, 1 otherwise
 */
int load_kernel_from_nand(char* partition, ulong addr, uint8_t* initramfs_header_buffer)
{
	int r;
	size_t cnt;
	u8 pnum;
	loff_t offset = 0;
	image_header_t* hdr;
	nand_info_t* nand;
	struct mtd_device* dev;
	struct part_info* part;

	if (mtdparts_init() != 0) {
		puts("mtdparts_init error\n");
		return 1;
	}

	if (find_dev_and_part(partition, &dev, &pnum, &part) != 0) {
		puts("find_dev_and_part error\n");
		return 1;
	}

	if (dev->id->type != MTD_DEV_TYPE_NAND) {
		puts("not a NAND\n");
		return 1;
	}

	nand = &nand_info[dev->id->num];
	offset = part->offset;

	printf("Loading from %s, offset 0x%08x\n", nand->name, (unsigned int)offset);

#if defined(__STB_SECURE__)
	cnt = IFS_HEADER_SIZE;
	r = nand_read_skip_bad(nand, offset, &cnt, (u_char *)initramfs_header_buffer);
	if (r) {
		puts("nand_read_opts error\n");
		return 1;
	}

	offset += cnt;
#endif

	cnt = IMAGE_HEADER_SIZE;
	r = nand_read_skip_bad(nand, offset, &cnt, (u_char *)addr);
	if (r) {
		puts("nand_read_opts error\n");
		return 1;
	}

	hdr = (image_header_t*)addr;

	printf("## Verifying Magic Number... ");
	if (ntohl(hdr->ih_magic) != IH_MAGIC) {
		printf("\n** Bad Magic Number 0x%x **\n", hdr->ih_magic);
		return 1;
	} else {
		printf("Ok\n");
	}

	image_print_contents(hdr);

	cnt = (ntohl(hdr->ih_size) + sizeof(image_header_t));
	r = nand_read_skip_bad(nand, offset, &cnt, (u_char *)addr);
	if (r) {
		puts("nand_read_opts error\n");
		return 1;
	}

	/* Loading ok, update default load address */
	load_addr = addr;
	printf("## Loading from NAND to RAM... Ok\n");

	return 0;
}

/**
 * Misc init functions (image validation)
 *
 * @return 0 on success, 1 otherwise
 */
int misc_init_r (void)
{
	char* value;
	int abort = 0;
	uint8_t initramfs_header_buffer[IFS_HEADER_SIZE];

/**
 * M.Schenk 2013.01.21
 * if env is not valid in case of factory, write the default values.
 */
#ifndef __ENV_SAVE_DEFAULTS__
	/* check if we have a valid env */
	if (env_check() != 0) {
		/**
		 * bad or no environment found
		 * slow blink (1 sec) on blue LED
		 */
		int toggle = 1;

		puts("!!! no valid environment found!\n");

		while (!abort) {
			int i;

			for (i = 0; !abort && i < 100; ++i) 	{
				/* Do we have got a key press? */
				if (tstc()) {
					abort = 1;
					setenv("bootcmd", " ");
					(void) getc();  /* consume input	*/
					STPIO_SET_PIN(STM_PIO_BASE(3), 4, 1); /* blue LED on */

					return 0;
				}

				/* 10ms delay per loop (total 100 loops) */
				mdelay(10);
			}

			/* let blink the blue LED */
			toggle = !toggle;
			STPIO_SET_PIN(STM_PIO_BASE(3), 4, toggle);
		}
	}
#else
	/* check if we have a valid env */
	if (env_check() != 0) {
		puts("!!! no valid environment found, write default values\n");
		mdelay(500);
		saveenv();
		puts("!!! default values written\n");
	}
#endif

	/* do no image check when booting over nfs */
	if (((value = getenv((char*)"bootcmd")) != NULL) && (strcmp(value,"run execnfsboot") == 0)) {
		puts ("NFS booting active, skipping image check\n");
		return 0;
	}

	/* force recovery boot check */
	if (stb_7108_force_recovery_boot_check() == 1) {
		puts("load recovery image/kernel from NAND to RAM\n");

		if (load_kernel_from_nand(KERNEL_RECOVERY_MTD_PART, KERNEL_RECOVERY_ADDRESS,
								  initramfs_header_buffer) == 0) {
			puts("force recovery boot\n");

			/* check recovery image */
			if(image_check(KERNEL_RECOVERY_ADDRESS, initramfs_header_buffer) == 0) {
				setenv("bootcmd", "run execrecoveryboot");
				return 0;
			}
		}
		else {
			puts("loading recovery image/kernel failed\n");
		}

		puts("no valid recovery image/kernel\n");
	}

	/* load main kernel from NAND to RAM */
	puts("load main image/kernel from NAND to RAM\n");

	if (load_kernel_from_nand(KERNEL_MAIN_MTD_PART, KERNEL_MAIN_ADDRESS,
							  initramfs_header_buffer) == 0) {

		puts("checking image/kernel image\n");

		/* check main image */
		if (image_check(KERNEL_MAIN_ADDRESS, initramfs_header_buffer) == 0) {
			puts("found valid image/kernel image\n");
			return 0;
		}

	}

	puts("no valid image/kernel image\n");

	/* load recovery kernel from NAND to RAM */
	puts("load recovery image/kernel from NAND to RAM\n");

	if (load_kernel_from_nand(KERNEL_RECOVERY_MTD_PART, KERNEL_RECOVERY_ADDRESS,
							  initramfs_header_buffer) == 0) {

		/* check recovery image */
		if (image_check(KERNEL_RECOVERY_ADDRESS, initramfs_header_buffer) == 0) {
			puts("!!! booting from recovery image/kernel !!!\n");
			setenv("bootcmd", "run execrecoveryboot");
			return 0;
		}
	}

	puts("no valid recovery image/kernel\n");

#if defined(__SEC_LEVEL_STRICT__)
  /* in strict mode no NFS functionality available. That means STB is end of life!! */
	puts("!!!! no valid images found, Box is dead !!!!!\n");
#else
	puts("no valid image/kernel found, force NFS booting\n");

	/*  both images are bad and bootcmd is not nfs, so try the best and activate nfs booting  */
	setenv("bootcmd", "run execnfsboot");
#endif

	return 0;
}

/**
 * Board video init function for splashscreen support
 *
 * @return 0 on success, 1 otherwise
 */
unsigned int board_video_init(void)
{
	return 0;
}

/**
 * Special reset cmd function
 *
 */
void do_reset (cmd_tbl_t * cmdtp, bd_t * bd, int flag, int argc, char *argv[])
{
	STPIO_SET_PIN(STM_PIO_BASE(3), 5, 0);
	SET_PIO_PIN(STM_PIO_BASE(3), 5, STPIO_OUT);

	/* wait for H/W reset to kick in ... */
	for (;;);
}

