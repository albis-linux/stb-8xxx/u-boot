/*
 * (C) Copyright 2013-2014 ALBIS Technologies.
 *
 * Michael Schenk <michael.schenk@albistechnologies.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <command.h>
#include <stm/soc.h>
#include <stm/stxh205reg.h>
#include <asm/io.h>
#include <stm/pio.h>
#if defined(CONFIG_CMD_I2C)
#	include <i2c.h>
#endif
#include "stb_h205-drivers.h"

#define PIOALT(port, pin, alt, dir)			\
do							\
{							\
	stxh205_pioalt_select((port), (pin), (alt));	\
	stxh205_pioalt_pad((port), (pin), (dir));	\
} while(0)

#define POWER_ON_ETH		3, 3	/* PIO3[3] == POWER_ON_ETH (a.k.a. ETH_RESET) */

/**
 * check recovery force booting switch (switch on bottom)
 *
 * @return 0 not pressed, 1 presses
 */
int stb_h205_force_recovery_boot_check(void)
{
	int cnt;

#if defined(STB_8083)
	/* standby switch is on 13/5 */
	if (STPIO_GET_PIN(STM_PIO_BASE(13), 5) != 0)
		return 0;

	cnt = RECOVER_BOOT_CHECK_TIME / RECOVER_BOOT_CHECK_INTERVAL;

	while (cnt-- != 0) {
		mdelay(RECOVER_BOOT_CHECK_INTERVAL);
		if (STPIO_GET_PIN(STM_PIO_BASE(13), 5) != 0)
			return 0;
	}
#elif defined(STB_8090)
	/* standby switch is on 2/3 */
	if (STPIO_GET_PIN(STM_PIO_BASE(2), 3) != 0)
		return 0;

	cnt = RECOVER_BOOT_CHECK_TIME / RECOVER_BOOT_CHECK_INTERVAL;

	while (cnt-- != 0) {
		mdelay(RECOVER_BOOT_CHECK_INTERVAL);
		if (STPIO_GET_PIN(STM_PIO_BASE(2), 3) != 0)
			return 0;
	}
#endif

	return 1;
}

#if defined(STB_8090)
/**
 * Init function for Audio/Video Switch Chip STV6430
 *
 * @return none
 */
static void stb_h205_stv6430_init(void)
{
	/*
	 * init i2c Bus
	 */
	i2c_init(CONFIG_SYS_I2C_SPEED, CONFIG_SYS_I2C_SLAVE);

	/*
	 * set init valus of the STV6430 Registers
	 */
	i2c_reg_write(STV6430_I2C_ADDRESS, STV6430_REG_WR0,
						   STV6430_REG_WR0_VALUE);
	i2c_reg_write(STV6430_I2C_ADDRESS, STV6430_REG_WR1,
						   STV6430_REG_WR1_VALUE);
	i2c_reg_write(STV6430_I2C_ADDRESS, STV6430_REG_WR2,
						   STV6430_REG_WR2_VALUE);
	i2c_reg_write(STV6430_I2C_ADDRESS, STV6430_REG_WR3,
						   STV6430_REG_WR3_VALUE);
	i2c_reg_write(STV6430_I2C_ADDRESS, STV6430_REG_WR4,
						   STV6430_REG_WR4_VALUE);
}
#endif

#ifdef CONFIG_DRIVER_NET_STM_GMAC
extern void stmac_phy_reset(void)
{
	/*
	 * Reset the Ethernet PHY.
	 * Note both PHYs share the *same* reset line.
	 *
	 *	PIO3[3] = POWER_ON_ETH (a.k.a. ETH_RESET)
	 */
	STPIO_SET_PIN(STM_PIO_BASE(3), 3, 0);
	udelay(10000);				/* 10 ms */
	STPIO_SET_PIN(STM_PIO_BASE(3), 3, 1);
	udelay(10000);				/* 10 ms */
}
#endif	/* CONFIG_DRIVER_NET_STM_GMAC */

/**
 * STB-h205 ethernet configuration
 *
 * @return none
 */
static void stb_h205_config_ethernet(void)
{
#ifdef CONFIG_DRIVER_NET_STM_GMAC
	/*
	 * Normally, we will use MII (or RMII) with an External Clock.
	 */
	stxh205_configure_ethernet(0, &(struct stxh205_ethernet_config) {
#if defined(CONFIG_STM_USE_RMII_MODE)
			.mode = stxh205_ethernet_mode_rmii,	/* RMII */
#else
			.mode = stxh205_ethernet_mode_mii,	/* MII */
#endif	/* CONFIG_STM_USE_RMII_MODE */
			.ext_clk = 1,					/* External Phy Clock */
			.phy_bus = 0, });
	/* Hard Reset the PHY -- do after we have configured the MAC */
	stmac_phy_reset();
#endif	/* CONFIG_DRIVER_NET_STM_GMAC */
}

/**
 * STB-h205 I2C configuration
 *
 * @return none
 */
static void stb_h205_config_i2c_busses(void)
{
	/**
	 * I2C bus 6 for HDMI
	 * ------------------------
	 * I2C_SCL6 is on 2/6 (output, high level)
	 * I2C_SDA6 is on 2/7 (input, high level)
	 */
	STPIO_SET_PIN(STM_PIO_BASE(2), 6, 1);
	STPIO_SET_PIN(STM_PIO_BASE(2), 7, 1);
	SET_PIO_PIN(STM_PIO_BASE(2), 6, STPIO_OUT);
	SET_PIO_PIN(STM_PIO_BASE(2), 7, STPIO_IN);


#if defined(STB_8090)
	/**
	 * I2C bus 3 for STV6430
	 * ------------------------
	 * I2C_SCL3 is on 15/5 (output, high level)
	 * I2C_SDA3 is on 15/6 (input, high level)
	 */
	STPIO_SET_PIN(STM_PIO_BASE(15), 5, 1);
	STPIO_SET_PIN(STM_PIO_BASE(15), 6, 1);
	SET_PIO_PIN(STM_PIO_BASE(15), 5, STPIO_OUT);
	SET_PIO_PIN(STM_PIO_BASE(15), 6, STPIO_IN);
#endif

#if defined(CONFIG_CMD_I2C)
	stxh205_configure_i2c();
#endif	/* CONFIG_CMD_I2C */
}

/**
 * STB-h205 board led configuration
 *
 * @return none
 */
void stb_h205_config_led(void)
{
#if defined(STB_8083)
	/**
	 * Power LEDs
	 * LED_GREEN Green connected to 3/0
	 * LED_RED Red connected to 3/1
	 */

	/* turn on green LED */
	STPIO_SET_PIN(STM_PIO_BASE(3), 0, 1);

	/* turn off red LED */
	STPIO_SET_PIN(STM_PIO_BASE(3), 1, 0);

	SET_PIO_PIN(STM_PIO_BASE(3), 0, STPIO_OUT);
	SET_PIO_PIN(STM_PIO_BASE(3), 1, STPIO_OUT);
#elif defined(STB_8090)
	/**
	 * Power LEDs
	 * LED_BLUE Blue connected to 10/4
	 * LED_RED Red connected to 10/3
	 */

	/* turn on blue LED */
	STPIO_SET_PIN(STM_PIO_BASE(10), 4, 1);

	/* turn off red LED */
	STPIO_SET_PIN(STM_PIO_BASE(10), 3, 0);

	SET_PIO_PIN(STM_PIO_BASE(10), 4, STPIO_OUT);
	SET_PIO_PIN(STM_PIO_BASE(10), 3, STPIO_OUT);

	/**
	 * Central LEDs
	 * LED_IR_W white connected to 10/0
	 * LED_IR_R red connected to 10/1
	 */

	/* turn off central LEDs */
	STPIO_SET_PIN(STM_PIO_BASE(10), 0, 0);
	STPIO_SET_PIN(STM_PIO_BASE(10), 1, 0);

	SET_PIO_PIN(STM_PIO_BASE(10), 0, STPIO_OUT);
	SET_PIO_PIN(STM_PIO_BASE(10), 1, STPIO_OUT);
#endif
}

/**
 * STB-h205 board pre init functions
 *
 * @return none
 */
void stb_h205_config_pre(void)
{
	/* Setup PIOs for ASC device */

#if CONFIG_SYS_STM_ASC_BASE == STXH205_ASC1_BASE

	/* Route UART #1 via PIO11 for TX & RX (JK1, 4-pin header) */
	PIOALT(11, 0, 3, stm_pad_direction_output);	/* UART1-TX */
	PIOALT(11, 1, 3, stm_pad_direction_input);	/* UART1-RX */
//	PIOALT(11, 2, 3, stm_pad_direction_output);	/* UART1-RTS */
//	PIOALT(11, 4, 3, stm_pad_direction_input);	/* UART1-CTS */

#elif CONFIG_SYS_STM_ASC_BASE == STXH205_ASC10_BASE

	/* Route UART #10 via PIO3 for TX & RX (JM5, DB9) */
	PIOALT(3, 5, 2, stm_pad_direction_output);	/* UART10-TX */
	PIOALT(3, 6, 2, stm_pad_direction_input);	/* UART10-RX */
//	PIOALT(3, 4, 2, stm_pad_direction_output);	/* UART10-RTS */
//	PIOALT(3, 7, 2, stm_pad_direction_input);	/* UART10-CTS */

#else
#error Unknown ASC port selected!
#endif	/* CONFIG_SYS_STM_ASC_BASE == STXH205_ASCx_BASE */

#ifdef CONFIG_DRIVER_NET_STM_GMAC
	/*
	 * Configure the Ethernet PHY Reset signal
	 */
	SET_PIO_PIN2(POWER_ON_ETH, STPIO_OUT);
#endif	/* CONFIG_DRIVER_NET_STM_GMAC */

#if 1
	/* disable USB power */
#ifndef CONFIG_STM_USB
#if defined(STB_8083)
	/* USB1 */
	STPIO_SET_PIN(STM_PIO_BASE(4), 5, 0);
	SET_PIO_PIN(STM_PIO_BASE(4), 5, STPIO_OUT);
#elif defined(STB_8090)
	/* USB0 */
	STPIO_SET_PIN(STM_PIO_BASE(4), 3, 0);
	SET_PIO_PIN(STM_PIO_BASE(4), 3, STPIO_OUT);

	/* USB1 */
	STPIO_SET_PIN(STM_PIO_BASE(4), 5, 0);
	SET_PIO_PIN(STM_PIO_BASE(4), 5, STPIO_OUT);
#endif
#endif
#endif

/* M.Schenk 2013.06.07 waiting for new PCB with a hardware reset */
	/**
	 * Hardware reset generation output
	 * -----------
	 * SW_RSTn is on 3/5 (input)
	 */
//	SET_PIO_PIN(STM_PIO_BASE(3), 5, STPIO_IN);

	/**
	 * NAND flash WP#
	 * -----------
	 * FLASH_WP is on 6/2 (output)
	 */
	SET_PIO_PIN(STM_PIO_BASE(6), 2, STPIO_OUT);
}

/**
 * STB-h205 board misc init functions
 *
 * @return none
 */
void stb_h205_config_misc(void)
{
	/**
	 * IR receiver
	 * -----------
	 * IR is on 3/4
	 */
	SET_PIO_PIN(STM_PIO_BASE(3), 4, STPIO_IN);
}

/**
 * STB-h205 board misc init functions
 *
 * @return none
 */
void stb_h205_config_power(void)
{
	/**
	 * Power standby control
	 * --------------------------------------
	 * POWER_CTRL is on 3/7 (output, high level = on)
	 */
	STPIO_SET_PIN(STM_PIO_BASE(3), 7, 1);
	SET_PIO_PIN(STM_PIO_BASE(3), 7, STPIO_OUT);
}

/**
 * STB-h205 board init function
 *
 * @return unused
 */
int stb_h205_board_init(void)
{
	stb_h205_config_power();

	stb_h205_config_pre();

	stb_h205_config_led();

	stb_h205_config_i2c_busses();

	/* Configure & Reset the Ethernet PHY */
	stb_h205_config_ethernet();

#if defined(STB_8090)
	stb_h205_stv6430_init();
#endif

	stb_h205_config_misc();

#if defined(CONFIG_STM_SATA)
	stx7105_configure_sata ();
#endif	/* CONFIG_STM_SATA */

	return 0;
}
