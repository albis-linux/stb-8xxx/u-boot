#
# (C) Copyright 2004-2013
# Andy Sturges, STMicroelectronics, andy.sturges@st.com
# Sean McGoogan STMicroelectronics, <Sean.McGoogan@st.com>
#
# See file CREDITS for list of people who contributed to this
# project.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston,
# MA 02111-1307 USA
#

ifneq ("$(wildcard $(PROJECT_ROOT)/int/project.include)","")
$(info inside Albis SVN, using $(PROJECT_ROOT)/int/project.include)
include $(PROJECT_ROOT)/int/project.include
include $(TOPDIR)/stm.mk

ifeq ("$(DVD_BACKEND)", "7105")
	ALBIS_VERSION = $(shell cat $(BOOTLOADER_ROOT)/STB_7105-version)
	ST40_IDENT_STRING1="\"$(ALBIS_VERSION)-$(PROFILETYPE)
	PLATFORM_CPPFLAGS += -DST_7105
endif

ifeq ("$(DVD_BACKEND)", "7108")
	ALBIS_VERSION = $(shell cat $(BOOTLOADER_ROOT)/STB_7108-version)
	ST40_IDENT_STRING1="\"$(ALBIS_VERSION)-$(PROFILETYPE)
	PLATFORM_CPPFLAGS += -DST_7108
endif

ifeq ("$(DVD_BACKEND)", "h205")
	#SDK42 is not used for h205.
	ALBIS_VERSION = $(shell cat $(BOOTLOADER_ROOT)/STB_h205-version)
	ST40_IDENT_STRING1="\"$(ALBIS_VERSION)-$(PROFILETYPE)
	PLATFORM_CPPFLAGS += -DST_H205
endif

ifeq ("$(SECLEVEL)", "strict")
	ST40_IDENT_STRING2=$(ST40_IDENT_STRING1)-str
else
ifeq ("$(SECLEVEL)", "test")
	ST40_IDENT_STRING2=$(ST40_IDENT_STRING1)-tst
else
	ST40_IDENT_STRING2=$(ST40_IDENT_STRING1)
endif
endif

ifeq ("$(BUILDTYPE)", "factoryloader")
	ST40_IDENT_STRING3=$(ST40_IDENT_STRING2)fl
else
	ST40_IDENT_STRING3=$(ST40_IDENT_STRING2)
endif

ST40_IDENT_STRING=$(ST40_IDENT_STRING3)\""

CROSS_COMPILE ?= sh4-linux-

sinclude $(OBJTREE)/include/autoconf.mk
STANDALONE_LOAD_ADDR = $(CONFIG_SYS_SDRAM_BASE)

PLATFORM_CPPFLAGS += -DCONFIG_ST40 -D__SH4__ -DCONFIG_IDENT_STRING=$(ST40_IDENT_STRING) $(PROFILEDEF) $(SECLEVELDEF) $(BUILDTYPEDEF)
PLATFORM_LDFLAGS  += -n

else
$(info outside Albis SVN)

include $(TOPDIR)/stm.mk

CROSS_COMPILE ?= sh4-linux-

sinclude $(OBJTREE)/include/autoconf.mk
STANDALONE_LOAD_ADDR = $(CONFIG_SYS_SDRAM_BASE)

PLATFORM_CPPFLAGS += -DCONFIG_ST40 -D__SH4__ -DCONFIG_IDENT_STRING=$(STM_IDENT_STRING)
PLATFORM_LDFLAGS  += -n


endif