# Albis STB-8xxx U-Boot

Taken from [8000.ZAP.01.XX.0641.37](http://opensource.albistechnologies.com/STB/STB-8000/8000.ZAP.01.XX.0641.37.htm)

### Usage

This U-Boot release needs gcc 4.x.  
[This STLinux toolchain](https://gitlab.com/albis-linux/toolchain) is tested and produces valid binaries.

Steps:
- `export ARCH=st40`
- `export CROSS_COMPILE=/path/to/toolchain/sh4-linux-`
- `make STB_8000se`
- load `u-boot` over JTAG or write `u-boot.bin` to flash
